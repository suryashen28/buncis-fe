const { merge } = require("webpack-merge");
const common = require("./webpack.common");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

module.exports = merge(common, {
  mode: "production",
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin(
        {
          sourceMap: true,
          terserOptions: {
            extractComments: "all"
          }
        },
        new OptimizeCSSAssetsPlugin({})
      )
    ],
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all"
        }
      },
      chunks: "all"
    }
  }
});
