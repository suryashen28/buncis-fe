import corporate from "@api-mock/mock-data/PR/modules/corporate";
import collaborationType from "@api-mock/mock-data/PR/modules/collaboration-type";
import collaborationStatistic from "@api-mock/mock-data/PR/modules/collaboration-statistic";
import dashboardPR from "@api-mock/mock-data/PR/modules/dashboard";
import collaboration from "@api-mock/mock-data/PR/modules/collaboration";
import corporateContact from "@api-mock/mock-data/PR/modules/corporate-contact";
import event from "@api-mock/mock-data/EEO/modules/event";
import auth from "@api-mock/mock-data/auth/auth";
import partnerType from "@api-mock/mock-data/EEO/modules/partner-type";
import partner from "@api-mock/mock-data/EEO/modules/partner";

export const datas = [
  ...collaborationType,
  ...corporate,
  ...collaborationStatistic,
  ...collaboration,
  ...corporateContact,
  ...event,
  ...dashboardPR,
  ...auth,
  ...partnerType,
  ...partner
];
