import config from "@config";
import { getExpireTokenTime } from "@utils/date";

const { auth } = config.urls.auth;

const login = {
  status: 200,
  message: "success",
  method: "POST",
  url: auth.login,
  body: {
    email: "test@bncc.net",
    password: "123"
  },
  data: {
    name: "Irfan",
    subdivision: "PR",
    tokenType: "Bearer",
    accessToken: "asd",
    expiresAt: getExpireTokenTime()
  }
};
const verify = {
  status: 200,
  message: "success",
  method: "GET",
  url: auth.verify
};

export default [login, verify];
