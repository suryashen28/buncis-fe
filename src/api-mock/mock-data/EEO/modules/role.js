import config from "@config";

const { role } = config.urls.EEO;

const getRoles = {
  status: 200,
  method: "GET",
  message: "success",
  url: role.getRoles,
  error: "",
  page: {
    page: 1,
    totalPage: 2
  },
  data: [
    {
      id: 1,
      name: `Speaker`
    },
    {
      id: 2,
      name: `Mentor`
    }
  ]
};

const deleteRole = {
  status: 200,
  method: "DELETE",
  message: "success",
  url: role.deleteRole(1),
  error: ""
};

const createRole = {
  status: 200,
  method: "POST",
  message: "success",
  url: role.createRole,
  error: "",
  data: {
    id: 10,
    name: "Speaker"
  }
};

const updateRole = {
  status: 200,
  method: "PATCH",
  message: "success",
  url: role.updateRole(2),
  error: "",
  data: {
    id: 2,
    name: "Speaker"
  }
};

export default [getRoles, deleteRole, createRole, updateRole];
