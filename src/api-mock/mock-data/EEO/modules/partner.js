import config from "@config";

const { partner } = config.urls.EEO;

const getPartners = {
  status: 200,
  method: "GET",
  message: "success",
  url: partner.getPartners,
  page: {
    page: 1,
    totalPage: 1,
    pageSize: 6
  },
  params: {
    page: 1
  },
  data: [
    {
      id: 1,
      name: "Bina Nusantara",
      partnerType: {
        id: 1,
        name: "University"
      }
    },
    {
      id: 2,
      name: "Tech in Asia",
      partnerType: {
        id: 1,
        name: "Media Partner"
      }
    }
  ]
};

const getPartnerById = {
  status: 200,
  method: "GET",
  message: "success",
  url: partner.getPartnerById(1),
  data: {
    id: 2,
    name: "Tech in Asia",
    partnerType: {
      id: 1,
      name: "Media Partner"
    }
  }
};

const createPartner = {
  status: 200,
  method: "GET",
  message: "success",
  url: partner.createPartner,
  body: {
    name: "asd",
    partnerTypeId: 1
  },
  data: {
    id: 2,
    name: "Tech in Asia",
    partnerType: {
      id: 1,
      name: "Media Partner"
    }
  }
};

const updatePartner = {
  status: 200,
  method: "GET",
  message: "success",
  url: partner.updatePartner(1),
  body: {
    name: "Media Partner",
    partnerTypeId: 1
  },
  data: {
    id: 1,
    name: "Sponsor"
  }
};

const deletePartner = {
  status: 200,
  method: "GET",
  message: "success",
  url: partner.deletePartner(1),
  error: ""
};

export default [
  getPartners,
  getPartnerById,
  createPartner,
  updatePartner,
  deletePartner
];
