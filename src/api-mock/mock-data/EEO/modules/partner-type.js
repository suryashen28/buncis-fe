import config from "@config";

const { partnerType } = config.urls.EEO;

const getPartnerTypes = {
  status: 200,
  method: "GET",
  message: "success",
  url: partnerType.getPartnerTypes,
  page: {
    page: 1,
    totalPage: 1,
    pageSize: 6
  },
  params: {
    page: 1
  },
  data: [
    {
      id: 1,
      name: "Media Partner"
    },
    {
      id: 2,
      name: "University"
    }
  ]
};

const getAllPartnerTypes = {
  status: 200,
  method: "GET",
  message: "success",
  url: partnerType.getPartnerTypes,
  data: [
    {
      id: 1,
      name: "Media Partner"
    },
    {
      id: 2,
      name: "University"
    }
  ]
};

const getPartnerTypeById = {
  status: 200,
  method: "GET",
  message: "success",
  url: partnerType.getPartnerTypeById(1),
  data: {
    id: 1,
    name: "Media Partner"
  }
};

const createPartnerType = {
  status: 200,
  method: "GET",
  message: "success",
  url: partnerType.createPartnerType,
  body: {
    name: "asd"
  },
  data: {
    id: 3,
    name: "Sponsor"
  }
};

const updatePartnerType = {
  status: 200,
  method: "GET",
  message: "success",
  url: partnerType.updatePartnerType(1),
  body: {
    name: "Media Partner"
  },
  data: {
    id: 1,
    name: "Sponsor"
  }
};

const deletePartnerType = {
  status: 200,
  method: "GET",
  message: "success",
  url: partnerType.deletePartnerType(1),
  error: ""
};

export default [
  getPartnerTypes,
  getAllPartnerTypes,
  getPartnerTypeById,
  createPartnerType,
  updatePartnerType,
  deletePartnerType
];
