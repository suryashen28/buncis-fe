import config from "@config";

const { major } = config.urls.EEO;

const getMajors = {
  status: 200,
  method: "GET",
  message: "success",
  url: major.getMajors,
  error: "",
  page: {
    page: 1,
    totalPage: 2
  },
  data: [
    {
      id: 1,
      name: `Computer Science`
    },
    {
      id: 2,
      name: `Industrial Engineering`
    }
  ]
};

const deleteMajor = {
  status: 200,
  method: "DELETE",
  message: "success",
  url: major.getMajors(1),
  error: ""
};

const createMajor = {
  status: 200,
  method: "POST",
  message: "success",
  url: major.createMajor,
  error: "",
  data: {
    id: 10,
    name: "Hospitality"
  }
};

const getMajorById = {
  status: 200,
  method: "GET",
  message: "success",
  url: major.getMajorById(1),
  error: "",
  data: {
    id: 1,
    name: "Food Technology"
  }
};

const updateMajor = {
  status: 200,
  method: "PATCH",
  message: "success",
  url: major.updateMajor(2),
  error: "",
  data: {
    id: 2,
    name: "Computer Science"
  }
};

export default [getMajors, deleteMajor, createMajor, getMajorById, updateMajor];
