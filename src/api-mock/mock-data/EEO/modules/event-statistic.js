import config from "@config";

const { eventStatistic } = config.urls.EEO;

const getEventParticipantStatisticByMonth = {
  status: 200,
  method: "GET",
  message: "success",
  params: {
    filterBy: "Month",
    period: 2020
  },
  url: eventStatistic.getEventParticipantStatistic,
  error: "",
  data: [
    { name: "January", total: 6000 },
    { name: "February", total: 2344 },
    { name: "March", total: 6542 },
    { name: "April", total: 2341 },
    { name: "May", total: 467 },
    { name: "June", total: 9773 },
    { name: "July", total: 2346 },
    { name: "August", total: 5643 },
    { name: "September", total: 8764 },
    { name: "October", total: 1233 },
    { name: "November", total: 8675 },
    { name: "December", total: 2345 }
  ]
};

const getEventParticipantStatisticByYear = {
  status: 200,
  method: "GET",
  message: "success",
  params: {
    filterBy: "Year",
    period: 2020
  },
  url: eventStatistic.getEventParticipantStatistic,
  error: "",
  data: [
    { name: "2011", total: 4533 },
    { name: "2012", total: 2345 },
    { name: "2013", total: 2343 },
    { name: "2014", total: 6543 },
    { name: "2015", total: 8777 },
    { name: "2016", total: 6532 },
    { name: "2017", total: 234 },
    { name: "2018", total: 6543 }
  ]
};

const getEventProfitStatisticByMonth = {
  code: 200,
  method: "GET",
  message: "success",
  params: {
    filterBy: "Month",
    period: 2020
  },
  url: eventStatistic.getEventProfitStatistic,
  error: "",
  data: [
    { name: "January", total: 1500000 },
    { name: "February", total: 950000 },
    { name: "March", total: 350000 },
    { name: "April", total: 555000 },
    { name: "May", total: 1607000 },
    { name: "June", total: 2500000 },
    { name: "July", total: 3500000 },
    { name: "August", total: 1945000 },
    { name: "September", total: 1010000 },
    { name: "October", total: 450000 },
    { name: "November", total: 350000 },
    { name: "December", total: 610000 }
  ]
};

const getEventProfitStatisticByYear = {
  code: 200,
  method: "GET",
  message: "success",
  params: {
    filterBy: "Year",
    period: 2020
  },
  url: eventStatistic.getEventProfitStatistic,
  error: "",
  data: [
    { name: "2011", total: 15000 },
    { name: "2012", total: 1000 },
    { name: "2013", total: 13500 },
    { name: "2014", total: 25000 },
    { name: "2015", total: 21250 },
    { name: "2016", total: 99500 },
    { name: "2017", total: 12000 },
    { name: "2018", total: 97800 }
  ]
};

export default [
  getEventParticipantStatisticByMonth,
  getEventParticipantStatisticByYear,
  getEventProfitStatisticByMonth,
  getEventProfitStatisticByYear
];
