import config from "@config";

const { eventDetail } = config.urls.EEO;

const createEventDetailNote = {
  status: 200,
  method: "POST",
  message: "success",
  url: eventDetail.createEventDetailNote(2),
  error: "",
  data: {
    id: 1,
    title: "Day 1",
    description: "Evaluation for Day 1"
  }
};

const getEventDetailNote = {
  status: 200,
  method: "GET",
  message: "success",
  url: eventDetail.getEventDetailNote(2),
  error: "",
  data: [
    {
      id: 1,
      title: "Day 1",
      description: "Evaluation for Day 1"
    },
    {
      id: 2,
      title: "Day 2",
      description: "Evaluation for Day 2"
    }
  ]
};

const updateEventDetailNote = {
  status: 200,
  method: "PATCH",
  message: "success",
  url: eventDetail.updateEventDetailNote(2, 1),
  error: "",
  data: {
    id: 1,
    title: "Day 1",
    description: "Evaluation for Day 1"
  }
};

const deleteEventDetailNote = {
  status: 200,
  method: "DELETE",
  message: "success",
  url: eventDetail.deleteEventDetailNote(2, 1),
  error: ""
};

const createEventDetailDocumentation = {
  status: 200,
  method: "POST",
  message: "success",
  url: eventDetail.createEventDetailDocumentation(2),
  error: "",
  data: [
    {
      id: 1,
      title: "Day1.jpg",
      fileUrl: "https://google.com"
    },
    {
      id: 2,
      title: "Day2.mp4",
      fileUrl: "https://google.com"
    }
  ]
};

const getEventDetailDocumentation = {
  status: 200,
  method: "GET",
  message: "success",
  url: eventDetail.getEventDetailDocumentation(2),
  error: "",
  data: [
    {
      id: 1,
      title: "Day1.jpg",
      fileUrl: "https://google.com"
    },
    {
      id: 2,
      title: "Day2.mp4",
      fileUrl: "https://google.com"
    }
  ]
};

const updateEventDetailDocumentation = {
  status: 200,
  method: "PATCH",
  message: "success",
  url: eventDetail.updateEventDetailDocumentation(2, 2),
  error: "",
  data: {
    id: 2,
    title: "Day2-Updated.mp4",
    fileUrl: "https://google.com"
  }
};

export default [
  createEventDetailNote,
  getEventDetailNote,
  updateEventDetailNote,
  deleteEventDetailNote,
  createEventDetailDocumentation,
  updateEventDetailDocumentation,
  getEventDetailDocumentation
];
