import config from "@config";

const { event } = config.urls.EEO;

const getAllEvents = {
  status: 200,
  method: "GET",
  message: "success",
  url: event.getEvents,
  error: "",
  data: [
    {
      id: 1,
      name: "BNCC Hackathon 2019"
    },
    {
      id: 2,
      name: "BNCC Idea Competition 2019"
    }
  ]
};

const getEvents = {
  status: 200,
  method: "GET",
  message: "success",
  url: event.getEvents,
  error: "",
  page: {
    page: 1,
    totalPage: 2
  },
  data: [
    {
      id: 3,
      name: `BNCC Technoscape`,
      startDateTime: `2020-08-16 08:00:01`,
      endDateTime: `2020-08-18 16:00:01`,
      description: `Mid year event on BNCC`,
      image: `https://lorempixel/Erdman PLC_1590212042.png`,
      totalParticipants: 5000,
      profit: 500000
    }
  ]
};

const getEventById = {
  status: 200,
  method: "GET",
  message: "success",
  url: event.getEventById(2),
  error: "",
  data: {
    id: 2,
    name: `BNCC Technoscape`,
    startDateTime: `2020-08-16 08:00:01`,
    endDateTime: `2020-08-18 16:00:01`,
    description: `Mid year event on BNCC`,
    image: `https://lorempixel/Erdman PLC_1590212042.png`,
    totalParticipants: 5000,
    profit: 500000
  }
};

const deleteEvent = {
  status: 200,
  method: "DELETE",
  message: "success",
  url: event.deleteEvent(3),
  error: ""
};

const updateEvent = {
  status: 200,
  method: "PATCH",
  message: "success",
  url: event.updateEvent(3),
  error: "",
  data: {
    id: 3,
    name: `BNCC Technoscape`,
    startDateTime: `2020-08-16 08:00:01`,
    endDateTime: `2020-08-18 16:00:01`,
    description: `Mid year event on BNCC`,
    image: `https://lorempixel/Erdman PLC_1590212042.png`,
    totalParticipants: 5000,
    profit: 500000
  }
};

const createEvent = {
  status: 200,
  method: "POST",
  message: "success",
  url: event.createEvent,
  error: "",
  data: {
    id: 3,
    name: `BNCC Technoscape`,
    startDateTime: `2020-08-16 08:00:01`,
    endDateTime: `2020-08-18 16:00:01`,
    description: `Mid year event on BNCC`,
    image: `https://lorempixel/Erdman PLC_1590212042.png`,
    totalParticipants: 5000,
    profit: 500000
  }
};

export default [
  getAllEvents,
  getEvents,
  getEventById,
  deleteEvent,
  updateEvent,
  createEvent
];
