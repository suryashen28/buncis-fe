import config from "@config";

const { participant } = config.urls.EEO;

const getParticipantsByPartnerId = {
  status: 200,
  method: "GET",
  message: "success",
  url: participant.getParticipantsByPartnerId(2, 1),
  error: "",
  page: {
    page: 1,
    totalPage: 2
  },
  data: [
    {
      id: 2,
      name: `Budi`,
      email: `budi@gmail.com`,
      phoneNumber: `08123123123`,
      doB: "2019 - 03 - 20",
      nim: 2201743322,
      major: {
        id: 1,
        name: `Information Technology`
      },
      role: {
        id: 1,
        name: `Speaker`
      },
      partner: {
        id: 1,
        name: `Bina Nusantara University`
      }
    }
  ]
};

const deleteParticipant = {
  status: 200,
  method: "DELETE",
  message: "success",
  url: participant.deleteParticipant(2, 2),
  error: ""
};

const createParticipants = {
  status: 200,
  method: "POST",
  message: "success",
  url: participant.createParticipants(2, 1),
  error: "",
  data: [
    {
      id: 2,
      name: `Budi`,
      email: `budi@gmail.com`,
      phoneNumber: `08123123123`,
      doB: "2019 - 03 - 20",
      nim: 2201743322,
      major: {
        id: 1,
        name: `Information Technology`
      },
      role: {
        id: 1,
        name: `Speaker`
      },
      partner: {
        id: 1,
        name: `Bina Nusantara University`
      }
    }
  ]
};

const updateParticipant = {
  status: 200,
  method: "PATCH",
  message: "success",
  url: participant.updateParticipant(2, 2),
  error: "",
  data: {
    id: 2,
    name: `Budi`,
    email: `budi@gmail.com`,
    phoneNumber: `08123123123`,
    doB: "2019 - 03 - 20",
    nim: 2201743322,
    major: {
      id: 1,
      name: `Information Technology`
    },
    role: {
      id: 1,
      name: `Speaker`
    },
    partner: {
      id: 1,
      name: `Bina Nusantara University`
    }
  }
};

const createParticipant = {
  status: 200,
  method: "POST",
  message: "success",
  url: participant.createParticipant(2),
  error: "",
  data: {
    id: 2,
    name: `Budi`,
    email: `budi@gmail.com`,
    phoneNumber: `08123123123`,
    doB: "2019 - 03 - 20",
    nim: 2201743322,
    major: {
      id: 1,
      name: `Information Technology`
    },
    role: {
      id: 1,
      name: `Speaker`
    },
    partner: {
      id: 1,
      name: `Bina Nusantara University`
    }
  }
};

const getParticipantById = {
  status: 200,
  method: "GET",
  message: "success",
  url: participant.getParticipantById(2, 1),
  error: "",
  data: {
    id: 2,
    name: `Budi`,
    email: `budi@gmail.com`,
    phoneNumber: `08123123123`,
    doB: "2019 - 03 - 20",
    nim: 2201743322,
    major: {
      id: 1,
      name: `Information Technology`
    },
    role: {
      id: 1,
      name: `Speaker`
    },
    partner: {
      id: 1,
      name: `Bina Nusantara University`
    }
  }
};

export default [
  getParticipantById,
  getParticipantsByPartnerId,
  deleteParticipant,
  createParticipant,
  createParticipants,
  updateParticipant
];
