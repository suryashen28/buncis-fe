import config from "@config";

const { dashboardEEO } = config.urls.EEO;

const getTopUniversityAttendee = {
  status: 200,
  method: "GET",
  message: "success",
  url: dashboardEEO.getTopUniversityAttendee,
  error: "",
  params: {
    size: 4
  },
  data: [
    {
      id: 2,
      name: "Universitas Pertanian Bogor",
      total: 500
    },
    {
      id: 1,
      name: "Institut Teknologi Bandung",
      total: 400
    },
    {
      id: 3,
      name: "Universitas Indonesia",
      total: 400
    },
    {
      id: 6,
      name: "Universitas Bina Nusantara",
      total: 300
    }
  ]
};

const getTopMajorAttendee = {
  status: 200,
  method: "GET",
  message: "success",
  url: dashboardEEO.getTopMajorAttendee,
  error: "",
  params: {
    size: 4
  },
  data: [
    {
      id: 2,
      name: "Computer Science",
      total: 500
    },
    {
      id: 1,
      name: "Information System",
      total: 150
    },
    {
      id: 3,
      name: "Industrial Engineering",
      total: 40
    },
    {
      id: 6,
      name: "Food Technology",
      total: 20
    }
  ]
};

const getRecentEvents = {
  status: 200,
  method: "GET",
  message: "success",
  url: dashboardEEO.getRecentEvents,
  error: "",
  data: [
    {
      id: 1,
      name: "BNCC Technoscape",
      date: "2019-10-02"
    },
    {
      id: 2,
      name: "BNCC Idea Competition",
      date: "2019-12-01"
    },
    {
      id: 3,
      name: "BNCC Virtual Conference",
      date: "2019-11-03"
    },
    {
      id: 4,
      name: "BNCC Webinar Series",
      date: "2019-11-04"
    }
  ]
};

const getCountPartner = {
  status: 200,
  method: "GET",
  message: "success",
  url: dashboardEEO.getCountPartner,
  error: "",
  data: {
    countPartner: 5000
  }
};

export default [
  getTopUniversityAttendee,
  getTopMajorAttendee,
  getRecentEvents,
  getCountPartner
];
