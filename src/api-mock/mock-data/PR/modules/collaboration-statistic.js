import config from "@config";

const { collaborationStatistic } = config.urls.PR;

const getCollaborationStatisticByMonth = {
  status: 200,
  method: "GET",
  message: "success",
  params: {
    filterBy: "Month",
    period: 2020
  },
  url: collaborationStatistic.getCollaborationStatistic,
  error: "",
  data: [
    { name: "January", total: 6000 },
    { name: "February", total: 2344 },
    { name: "March", total: 6542 },
    { name: "April", total: 2341 },
    { name: "May", total: 467 },
    { name: "June", total: 9773 },
    { name: "July", total: 2346 },
    { name: "August", total: 5643 },
    { name: "September", total: 8764 },
    { name: "October", total: 1233 },
    { name: "November", total: 8675 },
    { name: "December", total: 2345 }
  ]
};

const getCollaborationStatisticByYear = {
  status: 200,
  method: "GET",
  message: "success",
  params: {
    filterBy: "Year",
    period: 2020
  },
  url: collaborationStatistic.getCollaborationStatistic,
  error: "",
  data: [
    { name: "2011", total: 4533 },
    { name: "2012", total: 2345 },
    { name: "2013", total: 2343 },
    { name: "2014", total: 6543 },
    { name: "2015", total: 8777 },
    { name: "2016", total: 6532 },
    { name: "2017", total: 234 },
    { name: "2018", total: 6543 }
  ]
};

const getProfitStatisticByMonth = {
  code: 200,
  method: "GET",
  message: "success",
  params: {
    filterBy: "Month",
    period: 2020
  },
  url: collaborationStatistic.getProfitStatistic,
  error: "",
  data: [
    { name: "January", total: 1500000 },
    { name: "February", total: 950000 },
    { name: "March", total: 350000 },
    { name: "April", total: 555000 },
    { name: "May", total: 1607000 },
    { name: "June", total: 2500000 },
    { name: "July", total: 3500000 },
    { name: "August", total: 1945000 },
    { name: "September", total: 1010000 },
    { name: "October", total: 450000 },
    { name: "November", total: 350000 },
    { name: "December", total: 610000 }
  ]
};

const getProfitStatisticByYear = {
  code: 200,
  method: "GET",
  message: "success",
  params: {
    filterBy: "Year",
    period: 2020
  },
  url: collaborationStatistic.getProfitStatistic,
  error: "",
  data: [
    { name: "2011", total: 15000000 },
    { name: "2012", total: 10000000 },
    { name: "2013", total: 13500000 },
    { name: "2014", total: 25000000 },
    { name: "2015", total: 21250000 },
    { name: "2016", total: 9950000 },
    { name: "2017", total: 12000000 },
    { name: "2018", total: 978000 }
  ]
};

const getContactStatisticByMonth = {
  status: 200,
  method: "GET",
  message: "success",
  params: {
    filterBy: "Month",
    period: 2020
  },
  url: collaborationStatistic.getContactStatistic,
  error: "",
  data: [
    { name: "Jan", total: 2345 },
    { name: "Feb", total: 7865 },
    { name: "Mar", total: 4355 },
    { name: "Apr", total: 5532 },
    { name: "May", total: 675 },
    { name: "June", total: 123 },
    { name: "July", total: 865 },
    { name: "Aug", total: 7856 },
    { name: "Sep", total: 3444 },
    { name: "Oct", total: 8676 },
    { name: "Nov", total: 1234 },
    { name: "Dec", total: 123 }
  ]
};

const getContactStatisticByYear = {
  status: 200,
  method: "GET",
  message: "success",
  params: {
    filterBy: "Year",
    period: 2020
  },
  url: collaborationStatistic.getContactStatistic,
  error: "",
  data: [
    { name: 2011, total: 1234 },
    { name: 2012, total: 6534 },
    { name: 2013, total: 7876 },
    { name: 2014, total: 9878 },
    { name: 2015, total: 2344 },
    { name: 2016, total: 865 },
    { name: 2017, total: 524 },
    { name: 2018, total: 9786 }
  ]
};

export default [
  getCollaborationStatisticByMonth,
  getCollaborationStatisticByYear,
  getContactStatisticByMonth,
  getContactStatisticByYear,
  getProfitStatisticByMonth,
  getProfitStatisticByYear
];
