import config from "@config";

const { corporate } = config.urls.PR;

const getCorporates = {
  status: 200,
  method: "GET",
  message: "success",
  url: corporate.getCorporates,
  error: "",
  page: {
    page: 1,
    totalPage: 2,
    pageSize: 6
  },
  params: {
    page: 1
  },
  data: [
    {
      id: 1,
      address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk,  11532",
      name: "Telkom",
      image: "tel1.png",
      contactTotal: 1,
      collaborationTotal: 2
    },
    {
      id: 2,
      address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk,  11532",
      name: "ATS",
      image: "tel1.png",
      contactTotal: 10,
      collaborationTotal: 25
    },
    {
      id: 3,
      address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk, 11533",
      name: "HyperNet",
      image: "tel1.png",
      contactTotal: 20,
      collaborationTotal: 15
    },
    {
      id: 4,
      address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk, 11534",
      name: "Tokopedia",
      image: "tel1.png",
      contactTotal: 2,
      collaborationTotal: 15
    },
    {
      id: 5,
      address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk, 11535",
      name: "Astra",
      image: "tel1.png",
      contactTotal: 11,
      collaborationTotal: 13
    },
    {
      id: 6,
      address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk,  11536",
      name: "Tech In Asia",
      image: "tel1.png",
      contactTotal: 10,
      collaborationTotal: 20
    },
    {
      id: 7,
      address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk,  11537",
      name: "Kumparan",
      image: "tel1.png",
      contactTotal: 40,
      collaborationTotal: 25
    },
    {
      id: 8,
      address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk,  11538",
      name: "OVO",
      image: "tel1.png",
      contactTotal: 1,
      collaborationTotal: 2
    }
  ]
};

const getAllCorporates = {
  status: 200,
  method: "GET",
  message: "success",
  url: corporate.getCorporates,
  error: "",
  data: [
    {
      id: 1,
      name: "Telkom"
    },
    {
      id: 2,
      name: "ATS"
    },
    {
      id: 3,
      name: "HyperNet"
    },
    {
      id: 4,
      name: "Tokopedia"
    },
    {
      id: 5,
      name: "Astra"
    },
    {
      id: 6,
      name: "Tech In Asia"
    },
    {
      id: 7,
      name: "Kumparan"
    },
    {
      id: 8,
      name: "OVO"
    }
  ]
};

const getCorporateByID = {
  status: 200,
  method: "GET",
  message: "success",
  url: corporate.getCorporateByID(1),
  error: "",
  data: {
    id: 1,
    address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk,  11532",
    name: "Telkom",
    image: "tel1.png",
    contactTotal: 1,
    collaborationTotal: 2
  }
};

const createCorporate = {
  status: 200,
  message: "success",
  method: "POST",
  url: corporate.createCorporate,
  body: {
    name: "asd",
    address: "asd",
    image: "tel1.png"
  },
  data: {
    id: 23,
    address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk,  11532",
    name: "Telkom",
    image: "tel1.png",
    contactTotal: 1,
    collaborationTotal: 2
  }
};

const updateCorporate = {
  status: 200,
  message: "success",
  method: "PATCH",
  url: corporate.updateCorporate(1),
  error: "",
  body: {
    address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk,  11532",
    name: "Telkom",
    image: "tel1.png"
  },
  data: {
    id: 1,
    address: "Jl. Rw. Belong No.20, RT.1/RW.9, Kb. Jeruk,  11532",
    name: "Telkom mock",
    image: "tel1.png",
    contactTotal: 1,
    collaborationTotal: 2
  }
};

const deleteCorporate = {
  status: 200,
  message: "success",
  method: "DELETE",
  url: corporate.deleteCorporate(1),
  error: ""
};

export default [
  getCorporateByID,
  getCorporates,
  getAllCorporates,
  createCorporate,
  updateCorporate,
  deleteCorporate
];
