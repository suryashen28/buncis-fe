import config from "@config";

const { corporateContact } = config.urls.PR;

const getCorporateContacts = {
  status: 200,
  method: "GET",
  message: "success",
  url: corporateContact.getCorporateContacts,
  error: "",
  page: {
    page: 1,
    totalPage: 1,
    pageSize: 6
  },
  params: {
    page: 1,
    search: ""
  },
  data: [
    {
      id: 1,
      name: "Ronny Emard",
      phoneNumber: "081245628192",
      email: "conner.corkery@thiel.info",
      position: "Manager",
      corporate: {
        id: 1,
        name: "Telkomsel"
      }
    },
    {
      id: 2,
      name: "Roman",
      phoneNumber: "081245628192",
      email: "conner.corkery@thiel.info",
      position: "Security Engineer",
      corporate: {
        id: 2,
        name: "ATS"
      }
    },
    {
      id: 3,
      name: "Robert",
      phoneNumber: "081245628192",
      email: "conner.corkery@thiel.info",
      position: "PR",
      corporate: {
        id: 4,
        name: "Tokopedia"
      }
    },
    {
      id: 4,
      name: "Monica",
      phoneNumber: "081245628192",
      email: "conner.corkery@thiel.info",
      position: "Engineer Lead",
      corporate: {
        id: 2,
        name: "ATS"
      }
    },
    {
      id: 5,
      name: "Budi K",
      phoneNumber: "081245628192",
      email: "conner.corkery@thiel.info",
      position: "CEO",
      corporate: {
        id: 6,
        name: "Tech In Asia"
      }
    },
    {
      id: 6,
      name: "Bagas",
      phoneNumber: "081245628122",
      email: "conner.corkery@thiel.info",
      position: "Staff",
      corporate: {
        id: 7,
        name: "Kumparan"
      }
    }
  ]
};

const getAllCorporateContacts = {
  status: 200,
  method: "GET",
  message: "success",
  url: corporateContact.getCorporateContacts,
  error: "",
  data: [
    {
      id: 1,
      name: "Ronny Emard"
    },
    {
      id: 2,
      name: "Roman"
    },
    {
      id: 3,
      name: "Robert"
    },
    {
      id: 4,
      name: "Monica"
    },
    {
      id: 5,
      name: "Budi K"
    },
    {
      id: 6,
      name: "Bagas"
    }
  ]
};

const getCorporateContactByID = {
  status: 200,
  method: "GET",
  message: "success",
  url: corporateContact.getCorporateContactByID(1),
  error: "",
  data: {
    id: 1,
    name: "Ronny Emard",
    phoneNumber: "081245628192",
    email: "conner.corkery@thiel.info",
    position: "Manager",
    corporate: {
      id: 1,
      name: "Telkom"
    }
  }
};

const createCorporateContact = {
  status: 200,
  message: "success",
  method: "POST",
  url: corporateContact.createCorporateContact,
  body: {
    name: "asd",
    phoneNumber: "1234567891011",
    corporateID: 1,
    position: "asd",
    email: "asd@asd.com"
  },
  data: {
    id: 10,
    name: "Budi",
    phoneNumber: "0821119021232",
    corporate: {
      id: 1,
      name: "Tiket.com"
    },
    position: "asd",
    email: "asd"
  }
};

const updateCorporateContact = {
  status: 200,
  message: "success",
  method: "PATCH",
  url: corporateContact.updateCorporateContact(1),
  error: "",
  body: {
    name: "Ronny Emard",
    phoneNumber: "081245628192",
    corporateID: 1,
    position: "Manager",
    email: "conner.corkery@thiel.info"
  },
  data: {
    id: 1,
    name: "Ronny Emard",
    phoneNumber: "081245628192",
    email: "conner.corkery@thiel.info",
    position: "Manager",
    corporate: {
      id: 1,
      name: "Tiket.com"
    }
  }
};

const deleteCorporateContact = {
  status: 200,
  message: "success",
  method: "DELETE",
  url: corporateContact.deleteCorporateContact(1),
  error: ""
};

export default [
  getCorporateContacts,
  getAllCorporateContacts,
  getCorporateContactByID,
  createCorporateContact,
  updateCorporateContact,
  deleteCorporateContact
];
