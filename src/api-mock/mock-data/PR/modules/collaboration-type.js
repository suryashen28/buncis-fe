import config from "@config";

const { collaborationType } = config.urls.PR;

const getCollaborationType = {
  status: 200,
  method: "GET",
  message: "success",
  url: collaborationType.getCollaborationType,
  error: "",
  page: {
    page: 1,
    totalPage: 1,
    pageSize: 6
  },
  params: {
    page: 1
  },
  data: [
    {
      id: 1,
      type: "Sponsorship",
      description: "Sponsor"
    },
    {
      id: 2,
      type: "Media Partner",
      description: "Medpart untuk BNCC"
    }
  ]
};

const getAllCollaborationType = {
  code: 200,
  method: "GET",
  message: "success",
  url: collaborationType.getCollaborationType,
  error: "",
  data: [
    {
      id: 1,
      type: "Sponsorship",
      description: "Sponsor"
    },
    {
      id: 2,
      type: "Media Partner",
      description: "Medpart untuk BNCC"
    }
  ]
};

const getCollaborationTypeByID = {
  status: 200,
  method: "GET",
  message: "success",
  url: collaborationType.getCollaborationTypeByID(1),
  error: "",
  data: {
    id: 1,
    type: "Sponsorship",
    description: "Sponsor"
  }
};

const createCollaborationType = {
  statusus: 200,
  message: "success",
  method: "POST",
  url: collaborationType.createCollaborationType,
  body: {
    type: "asd",
    description: "asd"
  },
  data: {
    id: 3,
    type: "Sponsorship",
    description: "Sponsor"
  }
};

const updateCollaborationType = {
  status: 200,
  message: "success",
  method: "PATCH",
  url: collaborationType.updateCollaborationType(1),
  error: "",
  body: {
    type: "Sponsorship",
    description: "Sponsor"
  },
  data: {
    id: 1,
    type: "Sponsorship mock",
    description: "Sponsor"
  }
};

const deleteCollaborationType = {
  status: 200,
  message: "success",
  method: "DELETE",
  url: collaborationType.deleteCollaborationType(1),
  error: ""
};

export default [
  getCollaborationType,
  getAllCollaborationType,
  getCollaborationTypeByID,
  createCollaborationType,
  updateCollaborationType,
  deleteCollaborationType
];
