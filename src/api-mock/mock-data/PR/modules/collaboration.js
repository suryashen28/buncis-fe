import config from "@config";

import { parseToString, getTodayDate } from "@utils/date";

const { collaboration } = config.urls.PR;

const getCollaborations = {
  status: 200,
  method: "GET",
  message: "success",
  url: collaboration.getCollaborations,
  error: "",
  page: {
    page: 1,
    pageSize: 6,
    totalPage: 1
  },
  data: [
    {
      id: 1,
      description: "Collaboration with Tokopedia",
      date: parseToString(new Date("2020-03-20")),
      collaborationType: {
        id: 1,
        name: "Sponsor"
      },
      corporateContact: {
        id: 1,
        name: "Mugeni"
      },
      event: {
        id: 1,
        name: "BNCC Hackathon"
      },
      corporate: {
        id: 4,
        name: "Tokopedia"
      },
      profit: 3000
    },
    {
      id: 2,
      description: "Collaboration with Astra",
      date: parseToString(new Date("2020-02-20")),

      collaborationType: {
        id: 2,
        name: "Media Partner"
      },
      corporateContact: {
        id: 2,
        name: "Cahyo"
      },
      event: {
        id: 1,
        name: "BNCC Idea Competition"
      },
      corporate: {
        id: 5,
        name: "Astra"
      },
      profit: 50000
    },
    {
      id: 3,
      description: "Provider internet",
      date: parseToString(new Date("2019-02-20")),
      collaborationType: {
        id: 2,
        name: "Media Partner"
      },
      corporateContact: {
        id: 2,
        name: "Cahyo"
      },
      event: {
        id: 1,
        name: "BNCC Idea Competition"
      },
      corporate: {
        id: 1,
        name: "Telkom"
      },
      profit: 20000
    },
    {
      id: 4,
      description: "Provider Tiket",
      date: parseToString(new Date("2019-02-20")),
      collaborationType: {
        id: 1,
        name: "Sponsor"
      },
      corporateContact: {
        id: 2,
        name: "Cahyo"
      },
      corporate: {
        id: 5,
        name: "Astra"
      },
      profit: 1000000
    },
    {
      id: 5,
      description: "Provider Keyboard",
      date: parseToString(new Date("2018-02-20")),
      collaborationType: {
        id: 1,
        name: "Sponsor"
      },
      corporateContact: {
        id: 2,
        name: "Cahyo"
      },
      corporate: {
        id: 6,
        name: "Tech In Asia"
      },
      profit: 78000000
    }
  ]
};

const getCollaborationByID = {
  status: 200,
  method: "GET",
  message: "success",
  url: collaboration.getCollaborationByID(1),
  error: "",
  data: {
    id: 1,
    description: "Collaboration with Tokopedia",
    date: new Date("2020-03-20"),
    collaborationType: {
      id: 1,
      type: "Sponsor"
    },
    corporateContact: {
      id: 1,
      name: "Ronny Emard"
    },
    event: {
      id: 1,
      name: "BNCC Idea Competition"
    },
    corporate: {
      id: 4,
      name: "Tokopedia"
    },
    profit: 3000
  }
};

const createCollaboration = {
  status: 200,
  message: "success",
  method: "POST",
  url: collaboration.createCollaboration,
  body: {
    collaborationTypeID: 1,
    corporateID: 1,
    date: getTodayDate().getTime(),
    corporateContactID: 1,
    description: "asd",
    eventID: 1,
    profit: 3000
  },
  data: {
    id: 3,
    description: "Collaboration with Tokopedia",
    date: new Date("20-02-2020"),
    type: "Sponsor",
    corporateContact: {
      id: 1,
      name: "Ronny Emard"
    },
    event: {
      id: 1,
      name: "BNCC Hackathon 2019"
    },
    corporate: {
      id: 1,
      name: "Telkom"
    },
    profit: 3000
  }
};

const updateCollaboration = {
  status: 200,
  message: "success",
  method: "PATCH",
  url: collaboration.updateCollaboration(1),
  error: "",
  body: {
    collaborationTypeID: 1,
    corporateID: 4,
    date: new Date("2020-03-20").getTime(),
    corporateContactID: 1,
    description: "Collaboration with Tokopedia",
    eventID: 1,
    profit: 3000
  },
  data: {
    id: 1,
    collaboration: {
      description: "Collaboration with Tokopedia",
      date: new Date("2019-03-20"),
      type: "Sponsor"
    },
    corporateContact: {
      id: 1,
      name: "Ronny Emard"
    },
    event: {
      id: 1,
      name: "BNCC Hackathon 2019"
    },
    corporate: {
      id: 1,
      name: "Telkom"
    },
    profit: 3000
  }
};

const deleteCollaboration = {
  status: 200,
  message: "success",
  method: "DELETE",
  url: collaboration.deleteCollaboration(1),
  error: ""
};

export default [
  getCollaborations,
  createCollaboration,
  getCollaborationByID,
  updateCollaboration,
  deleteCollaboration
];
