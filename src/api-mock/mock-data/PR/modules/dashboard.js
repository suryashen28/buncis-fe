import config from "@config";

const { dashboardPR } = config.urls.PR;

const getCountCorporateContact = {
  status: 200,
  method: "GET",
  message: "success",
  url: dashboardPR.getCountCorporateContact,
  error: "",
  data: {
    countCorporateContacts: 67
  }
};

const getCountCollaborationThisYear = {
  status: 200,
  method: "GET",
  message: "success",
  url: dashboardPR.getCountCollaborationThisYear,
  error: "",
  data: {
    countCollaborationYear: 141
  }
};

const getRecentCollaboration = {
  status: 200,
  method: "GET",
  message: "success",
  url: dashboardPR.getRecentCollaboration,
  error: "",
  data: [
    {
      id: 1,
      name: "Budi",
      date: "2019-10-02"
    },
    {
      id: 2,
      name: "Andi",
      date: "2019-12-01"
    },
    {
      id: 3,
      name: "Anto",
      date: "2019-11-03"
    },
    {
      id: 4,
      name: "David",
      date: "2019-11-04"
    }
  ]
};

export default [
  getCountCorporateContact,
  getCountCollaborationThisYear,
  getRecentCollaboration
];
