import axios from "axios";
import MockAdapter from "axios-mock-adapter";

import { datas } from "@api-mock/mock-data/index";

const mock = new MockAdapter(axios);

const methodMap = {
  GET: "onGet",
  POST: "onPost",
  PUT: "onPut",
  PATCH: "onPatch",
  DELETE: "onDelete"
};

datas.forEach((data) => {
  const params = [data.url];
  switch (data.method) {
    case "GET": {
      params.push({ params: data.params });
      break;
    }
    case "POST": {
      params.push(data.body);
      break;
    }
    case "PUT": {
      params.push(data.body);
      break;
    }
    default:
      break;
  }
  mock[methodMap[data.method]](...params).reply(200, data);
});
