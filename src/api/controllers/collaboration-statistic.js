import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { collaborationStatistic } = config.urls.PR;
const accessToken = getAccessToken();

export default {
  getCollaborationStatistic(params) {
    return axios.get(collaborationStatistic.getCollaborationStatistic, {
      headers: {
        Authorization: accessToken
      },
      params
    });
  },
  getContactStatistic(params) {
    return axios.get(collaborationStatistic.getContactStatistic, {
      headers: {
        Authorization: accessToken
      },
      params
    });
  },
  getProfitStatistic(params) {
    return axios.get(collaborationStatistic.getProfitStatistic, {
      headers: {
        Authorization: accessToken
      },
      params
    });
  }
};
