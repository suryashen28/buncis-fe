import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { partnerType } = config.urls.EEO;
const accessToken = getAccessToken();

export default {
  getPartnerTypes(params) {
    return axios.get(partnerType.getPartnerTypes, {
      headers: {
        Authorization: accessToken
      },
      params: params
    });
  },
  createPartnerType(data) {
    return axios.post(partnerType.createPartnerType, data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getPartnerTypeById(id) {
    return axios.get(partnerType.getPartnerTypeById(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  updatePartnerType(id, data) {
    return axios.patch(partnerType.updatePartnerType(id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  deletePartnerType(id) {
    return axios.delete(partnerType.deletePartnerType(id), {
      headers: {
        Authorization: accessToken
      }
    });
  }
};
