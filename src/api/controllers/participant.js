import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { participant } = config.urls.EEO;
const accessToken = getAccessToken();

export default {
  updateParticipant(eventId, id, data) {
    return axios.patch(participant.updateParticipant(eventId, id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  createParticipant(eventId, data) {
    return axios.post(participant.createParticipant(eventId), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getParticipantById(eventId, id) {
    return axios.get(participant.getParticipantById(eventId, id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getParticipantsByPartnerTypeId(eventId, partnerTypeId) {
    return axios.get(
      participant.getParticipantsByPartnerTypeId(eventId, partnerTypeId),
      {
        headers: {
          Authorization: accessToken
        }
      }
    );
  },
  deleteParticipant(eventId, id) {
    return axios.delete(participant.deleteParticipant(eventId, id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  createParticipants(eventId, partnerTypeId, data) {
    return axios.post(
      participant.createParticipants(eventId, partnerTypeId),
      data,
      {
        headers: {
          Authorization: accessToken
        }
      }
    );
  }
};
