import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const accessToken = getAccessToken();
const { collaboration } = config.urls.PR;

export default {
  getCollaborations(params) {
    return axios.get(collaboration.getCollaborations, {
      headers: {
        Authorization: accessToken
      },
      params: params
    });
  },
  createCollaboration(data) {
    return axios.post(collaboration.createCollaboration, data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getCollaborationByID(id) {
    return axios.get(collaboration.getCollaborationByID(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  updateCollaboration(id, data) {
    return axios.patch(collaboration.deleteCollaboration(id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  deleteCollaboration(id) {
    return axios.delete(collaboration.deleteCollaboration(id), {
      headers: {
        Authorization: accessToken
      }
    });
  }
};
