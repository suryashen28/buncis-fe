import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { role } = config.urls.EEO;
const accessToken = getAccessToken();

export default {
  getRoles() {
    return axios.get(role.getRoles, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  deleteRole(id) {
    return axios.delete(role.deleteRole(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  createRole(data) {
    return axios.post(role.createRole, data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  updateRole(id, data) {
    return axios.patch(role.updateRole(id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  }
};
