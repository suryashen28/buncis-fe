import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";
import { getThisYear } from "@utils/date";

const { dashboardPR } = config.urls.PR;
const accessToken = getAccessToken();
const thisYear = getThisYear();

export default {
  getCountCorporateContact() {
    return axios.get(dashboardPR.getCountCorporateContact, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getCountCollaborationThisYear() {
    return axios.get(dashboardPR.getCountCollaborationThisYear, {
      headers: {
        Authorization: accessToken
      },
      params: { year: thisYear }
    });
  },
  getRecentCollaboration() {
    return axios.get(dashboardPR.getRecentCollaboration, {
      headers: {
        Authorization: accessToken
      }
    });
  }
};
