import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { corporateContact } = config.urls.PR;

const accessToken = getAccessToken();

export default {
  getCorporateContacts(params) {
    return axios.get(corporateContact.getCorporateContacts, {
      headers: {
        Authorization: accessToken
      },
      params: params
    });
  },
  getCorporateContactByID(id) {
    return axios.get(corporateContact.getCorporateContactByID(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  async createCorporateContact(data) {
    const datas = await axios
      .post(corporateContact.createCorporateContact, data, {
        headers: {
          Authorization: accessToken
        }
      })
      .catch((e) => e);
    return datas;
  },
  updateCorporateContact(id, data) {
    return axios.patch(corporateContact.updateCorporateContact(id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  deleteCorporateContact(id) {
    return axios.delete(corporateContact.deleteCorporateContact(id), {
      headers: {
        Authorization: accessToken
      }
    });
  }
};
