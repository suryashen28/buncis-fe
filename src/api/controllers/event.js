import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { event } = config.urls.EEO;
const accessToken = getAccessToken();

export default {
  getEvents() {
    return axios.get(event.getEvents, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  deleteEvent(id) {
    return axios.delete(event.deleteEvent(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  updateEvent(id, data) {
    return axios.patch(event.updateEvent(id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  createEvent(data) {
    return axios.post(event.createEvent, data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getEventById(id) {
    return axios.get(event.getEventById(id), {
      headers: {
        Authorization: accessToken
      }
    });
  }
};
