import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { partner } = config.urls.EEO;
const accessToken = getAccessToken();

export default {
  getPartners(params) {
    return axios.get(partner.getPartners, {
      headers: {
        Authorization: accessToken
      },
      params: params
    });
  },
  createPartner(data) {
    return axios.post(partner.createPartner, data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getPartnerById(id) {
    return axios.get(partner.getPartnerById(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  updatePartner(id, data) {
    return axios.patch(partner.updatePartner(id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  deletePartner(id) {
    return axios.delete(partner.deletePartner(id), {
      headers: {
        Authorization: accessToken
      }
    });
  }
};
