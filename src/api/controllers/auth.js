import axios from "axios";

import config from "@config";

import { getAccessToken } from "@utils/http-api";
import url from "@config/app";

const accessToken = getAccessToken();
const { auth } = config.urls.auth;

export default {
  async login(data) {
    const response = await axios.post(auth.login, data).catch((e) => e);
    return response;
  },
  async verifyUser() {
    const response = await axios
      .get(auth.verify, {
        headers: {
          Authorization: accessToken
        }
      })
      .then((res) => res.status)
      .catch((e) => e.response.status);
    return response;
  }
};
