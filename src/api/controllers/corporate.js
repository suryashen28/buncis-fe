import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { corporate } = config.urls.PR;
const accessToken = getAccessToken();

export default {
  getCorporates(params) {
    return axios.get(corporate.getCorporates, {
      headers: {
        Authorization: accessToken
      },
      params: params
    });
  },
  createCorporate(data) {
    return axios.post(corporate.createCorporate, data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getCorporateByID(id) {
    return axios.get(corporate.getCorporateByID(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  deleteCorporate(id) {
    return axios.delete(corporate.deleteCorporate(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  updateCorporate(id, data) {
    return axios.post(corporate.updateCorporate(id), data, {
      headers: {
        Authorization: accessToken,
        "Content-Type": "multipart/form-data"
      }
    });
  }
};
