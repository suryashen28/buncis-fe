import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { major } = config.urls.EEO;
const accessToken = getAccessToken();

export default {
  getMajors() {
    return axios.get(major.getMajors, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  createMajor(data) {
    return axios.post(major.createMajor, data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  deleteMajor(id) {
    return axios.delete(major.deleteMajor(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getMajorById(id) {
    return axios.get(major.getMajorById(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  updateMajor(id, data) {
    return axios.patch(major.updateMajor(id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  }
};
