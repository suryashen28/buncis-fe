import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { eventDetail } = config.urls.EEO;
const accessToken = getAccessToken();

export default {
  createEventDetailNote(id, data) {
    return axios.post(eventDetail.createPartner(id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getEventDetailNote(id) {
    return axios.get(eventDetail.getEventDetailNote(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  updateEventDetailNote(id, noteId, data) {
    return axios.patch(eventDetail.updateEventDetailNote(id, noteId), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  deleteEventDetailNote(id, noteId) {
    return axios.delete(eventDetail.deleteEventDetailNote(id, noteId), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  createEventDetailDocumentation(id, data) {
    return axios.post(eventDetail.createEventDetailDocumentation(id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getEventDetailDocumentation(id) {
    return axios.get(eventDetail.getEventDetailDocumentation(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  updateEventDetailDocumentation(id, documentationId, data) {
    return axios.patch(
      eventDetail.updateEventDetailNote(id, documentationId),
      data,
      {
        headers: {
          Authorization: accessToken
        }
      }
    );
  }
};
