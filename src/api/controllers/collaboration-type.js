import axios from "axios";

import config from "@config";
import { getAccessToken } from "@utils/http-api";

const { collaborationType } = config.urls.PR;
const accessToken = getAccessToken();

export default {
  getCollaborationType(params) {
    return axios.get(collaborationType.getCollaborationType, {
      headers: {
        Authorization: accessToken
      },
      params: params
    });
  },
  createCollaborationType(data) {
    return axios.post(collaborationType.createCollaborationType, data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  getCollaborationTypeByID(id) {
    return axios.get(collaborationType.getCollaborationTypeByID(id), {
      headers: {
        Authorization: accessToken
      }
    });
  },
  updateCollaborationType(id, data) {
    return axios.patch(collaborationType.updateCollaborationType(id), data, {
      headers: {
        Authorization: accessToken
      }
    });
  },
  deleteCollaborationType(id) {
    return axios.delete(collaborationType.deleteCollaborationType(id), {
      headers: {
        Authorization: accessToken
      }
    });
  }
};
