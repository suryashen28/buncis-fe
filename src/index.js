import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";

import App from "@src/App";
import store from "@redux/store";
import config from "@config";
import * as serviceWorker from "@src/serviceWorker";

process.env.NODE_ENV === config.environment.DEVELOPMENT &&
  require("./api-mock");

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

module.hot.accept();
serviceWorker.unregister();
