import React from "react";

const Card = ({ card: { color, icon, total, name, path }, history }) => {
  return (
    <div className="card bg-white">
      <div className="card-body">
        <div className="row card-detail">
          <div className="col-6">
            <h1 className={`card-title ${color}`}>{total}</h1>
            <h6 className="card-subtitle mb-2">{name}</h6>
          </div>
          <div className="col-6">
            <h1 className="icon">
              <i className={`fa fa-${icon} ${color}`} />
            </h1>
          </div>
        </div>
        <div className={`card-footer bg-${color}`}>
          <div className="card-footer-content">
            <h6 onClick={() => history.push(`${path}`)}>View More</h6>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
