import React from "react";
import { withRouter } from "react-router-dom";

import Modal from "@components/common/modal/Modal";

const CollaborationActions = ({ data, onDeleteColaboration }) => {
  const modalProps = (
    <div className="item-right delete">
      <div className="btn-delete">
        <i className="fa fa-trash" aria-hidden="true"></i>
        <span>Delete</span>
      </div>
    </div>
  );

  const modalContent = (
    <div className="container">
      <div className="container-header">Delete Data</div>
      <button
        className="fa fa-times modal-close close btn btn-none"
        aria-hidden="true"
      />
      <div className="sub-header small">Collaboration List</div>

      <div className="container-middle">{`${data.type}-${data.name}`}</div>
      <div className="container-footer">
        <span>Are you sure you want to delete this data ?</span>
        <div
          className="btn btn-success"
          onClick={() => onDeleteColaboration(data.id)}
        >
          <i className="fa fa-check" aria-hidden="true"></i>
        </div>
      </div>
    </div>
  );

  const renderModal = (
    <Modal modalProps={modalProps} modalContent={modalContent} />
  );

  return (
    <div className="details">
      <div className="item-left edit">
        <i className="fa fa-pencil" aria-hidden="true"></i>
        <span>Edit</span>
      </div>
      {renderModal()}
    </div>
  );
};

export default withRouter(CollaborationActions);
