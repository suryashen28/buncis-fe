import React from "react";

import Modal from "@components/common/modal/Modal";

const CollaborationModal = ({ onExit, handleDelete, collaboration }) => {
  const modalContent = () => {
    const { corporate, collaborationType, id } = collaboration;
    return (
      <div className="container">
        <div className="container-header">Delete Data</div>

        <div className="sub-header small">Collaboration List</div>
        <div className="container-middle">
          {` ${collaborationType.name} -
            ${corporate.name}`}
        </div>
        <div className="container-footer">
          <span>Are you sure you want to delete this data ?</span>
          <div
            className="btn btn-success"
            onClick={() => {
              handleDelete(id);
              onExit();
            }}
          >
            <i className="fa fa-check" aria-hidden="true" />
          </div>
        </div>
      </div>
    );
  };

  const modalActions = () => <i className="fa fa-trash red option" />;

  return (
    <Modal
      modalActions={modalActions()}
      modalContent={modalContent()}
      onExit={onExit}
    />
  );
};

export default CollaborationModal;
