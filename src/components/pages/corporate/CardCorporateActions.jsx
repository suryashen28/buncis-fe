import React from "react";
import { withRouter } from "react-router-dom";

import CorporateModal from "@components/pages/corporate/CorporateModal";
import routeList from "@src/route";

const CardCorporateActions = ({ card, onDeleteCorporate, history }) => {
  const { corporate } = routeList;

  const renderModal = () => (
    <CorporateModal
      corporate={card}
      history={history}
      handleDelete={onDeleteCorporate}
    />
  );

  return (
    <div className="card-actions">
      <div
        className="details action-edit"
        onClick={() => history.push(`${corporate.path}/${card.id}/edit`)}
      >
        <i className="fa fa-pencil" aria-hidden="true"></i>
        <span>Edit</span>
      </div>
      {renderModal()}
    </div>
  );
};

export default withRouter(CardCorporateActions);
