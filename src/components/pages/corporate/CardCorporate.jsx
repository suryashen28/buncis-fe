import React from "react";

import CardCorporateDetails from "@components/pages/corporate/CardCorporateDetails";
import CardCorporateActions from "@components/pages/corporate/CardCorporateActions";
import defaultCardImage from "@assets/images/default.png";

const CardCorporate = ({ card, onDeleteCorporate, history }) => {
  return (
    <div className="card-container">
      <div className="card-content">
        <div className="card-information">
          <img
            className="card-image"
            src={`${card.image}`}
            onError={(e) => (e.target.src = defaultCardImage)}
            alt="corporate"
          />
          <div className="card-information-header">
            <div className="card-title">{card.name}</div>
            <CardCorporateDetails card={card} history={history} />
          </div>
        </div>

        <CardCorporateActions
          card={card}
          onDeleteCorporate={onDeleteCorporate}
          name="card"
        />
      </div>
    </div>
  );
};

export default CardCorporate;
