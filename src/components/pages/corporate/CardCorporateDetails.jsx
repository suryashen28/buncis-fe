import React from "react";

import routeList from "@src/route";

const CardCorporateDetails = ({ card, history }) => {
  const { corporateContact, collaboration } = routeList;
  const contactUrlParams = `query=${card.name}&filterBy=corporate&page=1`;
  const collaborationUrlParams = `query=${card.name}&filterBy=corporate&page=1`;
  return (
    <div className="card-details">
      <div
        className="card-details-item-contact card-icon"
        onClick={() =>
          history.push({
            pathname: corporateContact.path,
            search: contactUrlParams,
            state: {
              id: card.id
            }
          })
        }
      >
        <i className="fa fa-address-book" aria-hidden="true"></i>
        <span>{card.contactTotal}</span>
      </div>
      <div
        className="card-details-item-collab card-icon"
        onClick={() =>
          history.push({
            pathname: collaboration.path,
            search: collaborationUrlParams,
            state: {
              id: card.id
            }
          })
        }
      >
        <i className="fa fa-handshake-o" aria-hidden="true"></i>
        <span>{card.collaborationTotal}</span>
      </div>
    </div>
  );
};

export default CardCorporateDetails;
