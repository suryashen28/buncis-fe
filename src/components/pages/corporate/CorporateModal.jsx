import React from "react";

import Modal from "@components/common/modal/Modal";
import config from "@config";
import defaultCardImage from "@assets/images/default.png";

const host = config.app.hostImagePath;

const CorporateModal = ({ corporate, handleDelete }) => {
  const modalActions = () => (
    <div className="details action-delete">
      <i className="fa fa-trash" aria-hidden="true"></i>
      <span>Delete</span>
    </div>
  );

  const modalContent = () => (
    <div className="container">
      <div className="container-header">Delete Data</div>

      <div className="sub-header small">Corporate List</div>

      <div className="container-middle">
        <img
          className="image"
          src={`${host}/${corporate.image}`}
          onError={(e) => (e.target.src = defaultCardImage)}
          alt="corporate"
        />
        {corporate.name}
      </div>
      <div className="container-footer">
        <span>Are you sure you want to delete this data ?</span>
        <div
          className="btn btn-success"
          onClick={() => {
            handleDelete(corporate.id);
          }}
        >
          <i className="fa fa-check" aria-hidden="true"></i>
        </div>
      </div>
    </div>
  );

  return <Modal modalActions={modalActions()} modalContent={modalContent()} />;
};

export default CorporateModal;
