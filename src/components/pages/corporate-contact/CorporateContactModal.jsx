import React from "react";

import Modal from "@components/common/modal/Modal";
import routeList from "@src/route";

const CorporateContactModal = ({
  onExit,
  history,
  handleDelete,
  corporateContact
}) => {
  const modalContent = () => {
    const { corporate, name, id } = corporateContact;
    return (
      <div className="container">
        <div className="container-header">Delete Data</div>
        <div className="sub-header small">Corporate Contact</div>
        <div className="container-middle">
          {` ${name} -
            ${corporate.name}`}
        </div>
        <div className="container-footer">
          <span>Are you sure you want to delete this data ?</span>
          <div
            className="btn btn-success"
            onClick={() => {
              handleDelete(id);
              onExit();
            }}
          >
            <i className="fa fa-check" aria-hidden="true" />
          </div>
        </div>
      </div>
    );
  };

  const modalActions = () => (
    <i
      className="fa fa-trash red option"
      onClick={() => {
        history.push({
          pathname: `${routeList.corporateContact.path}/edit`,
          search: `corporateContact=${corporateContact.id}`
        });
      }}
    />
  );

  return (
    <Modal
      modalActions={modalActions()}
      modalContent={modalContent()}
      onExit={onExit}
    />
  );
};

export default CorporateContactModal;
