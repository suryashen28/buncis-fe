import React from "react";

import Modal from "@components/common/modal/Modal";

const CollaborationTypeModal = ({
  onExit,
  collaborationType,
  handleDelete
}) => {
  const modalContent = () => {
    const { id, type } = collaborationType;
    return (
      <div className="container">
        <div className="container-header">Delete Data</div>
        <div className="sub-header small">Collaboration Type</div>
        <div className="container-middle">{`${type}`}</div>
        <div className="container-footer">
          <span>Are you sure you want to delete this data ?</span>
          <div
            className="btn btn-success"
            onClick={() => {
              handleDelete(id);
            }}
          >
            <i className="fa fa-check" aria-hidden="true" />
          </div>
        </div>
      </div>
    );
  };
  const modalActions = () => <i className="fa fa-trash red option" />;

  return (
    <Modal
      modalActions={modalActions()}
      modalContent={modalContent()}
      onExit={onExit}
    />
  );
};

export default CollaborationTypeModal;
