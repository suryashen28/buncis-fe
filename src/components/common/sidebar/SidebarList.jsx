import React from "react";
import SidebarSubmenuActive from "@components/common/sidebar/sidebar-submenu/SidebarSubmenuActive";
import SidebarSubmenuInactive from "@components/common/sidebar/sidebar-submenu/SidebarSubmenuInactive";

const SidebarList = ({
  sidebar,
  isActiveMenu,
  isActiveSubMenu,
  handleClassActive
}) => {
  const { subMenu } = sidebar;
  return subMenu ? (
    <SidebarSubmenuActive
      sidebar={sidebar}
      isActiveMenu={isActiveMenu}
      isActiveSubMenu={isActiveSubMenu}
      handleClassActive={handleClassActive}
    />
  ) : (
    <SidebarSubmenuInactive
      sidebar={sidebar}
      isActiveMenu={isActiveMenu}
      handleClassActive={handleClassActive}
    />
  );
};

export default SidebarList;
