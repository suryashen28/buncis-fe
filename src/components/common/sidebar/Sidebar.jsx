import React, { Component } from "react";

import SidebarList from "@components/common/sidebar/SidebarList";
import UserProfile from "@components/common/navbar/user-profile/UserProfile";
import { getThisYear } from "@utils/date";
import config from "@config/index";
import routeList from "@src/route";
import buncisLogo from "@assets/images/buncis_logo_white.png";

const publicUrl = config.app.currentHostImagePath;
const {
  dashboard,
  corporate,
  corporateContact,
  collaboration,
  collaborationType
} = routeList;
class Sidebar extends Component {
  state = {
    sidebars: [
      {
        id: 1,
        sidebarName: "Dashboard",
        sidebarIcon: "dashboard",
        link: dashboard.path
      },
      {
        id: 2,
        sidebarName: "Corporate",
        sidebarIcon: "building",
        link: corporate.path,
        classActive: false,
        subMenu: [
          {
            id: 1,
            sidebarName: "Corporate List",
            sidebarIcon: "address-book",
            link: corporate.path
          },
          {
            id: 2,
            sidebarName: "Corporate Contact",
            sidebarIcon: "address-book",
            link: corporateContact.path
          }
        ]
      },
      {
        id: 3,
        sidebarName: "Collaboration",
        sidebarIcon: "user-plus",
        link: collaboration.path,
        classActive: false,
        subMenu: [
          {
            id: 1,
            sidebarName: "Collaboration List",
            sidebarIcon: "signal",
            link: collaboration.path
          },
          {
            id: 2,
            sidebarName: "Collaboration Type",
            sidebarIcon: "users",
            link: collaborationType.path
          }
        ]
      }
    ],
    classActive: "default"
  };

  updateClassActive = (sidebarName) => {
    this.setState({ classActive: sidebarName });
  };

  isActiveMenu = (value) =>
    value === this.state.classActive ? "active-sidebar theme" : "default";

  isActiveSubMenu = (value) =>
    value === this.state.classActive ? "show-dropdown" : "hidden-dropdown";

  render() {
    const { isActiveMenu, isActiveSubMenu, updateClassActive } = this;
    const { sidebars } = this.state;
    const { status } = this.props;

    const renderSidebarList = () => {
      return sidebars.map((sidebar) => (
        <SidebarList
          key={sidebar.id}
          isActiveMenu={isActiveMenu}
          isActiveSubMenu={isActiveSubMenu}
          handleClassActive={updateClassActive}
          sidebar={sidebar}
        />
      ));
    };

    return (
      <div className={`sidebar-box ${status}`}>
        <div className="sidebar">
          <div className="buncis-logo-box">
            <img
              className="sidebar-buncis-logo"
              src={buncisLogo}
              alt="BUNCIS, BNCC Information System"
            />
          </div>
          <div>
            <UserProfile url={publicUrl} />
          </div>
          <div>
            <p className="sidebar-caption">MAIN NAVIGATION</p>
            {renderSidebarList()}
          </div>
          <div className="sidebar-copyright">
            &copy; {getThisYear()} Bina Nusantara Computer Club,
            <br />
            All Right Reserved
            <br />
            Version 0.1
          </div>
        </div>
      </div>
    );
  }
}

export default Sidebar;
