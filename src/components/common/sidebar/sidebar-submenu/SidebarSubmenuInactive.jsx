import React from "react";
import { Link } from "react-router-dom";

const SidebarSubmenuInactive = ({
  sidebar,
  isActiveMenu,
  handleClassActive
}) => {
  const { sidebarName, link } = sidebar;
  const isActiveMenus = isActiveMenu(sidebarName);
  const url =
    window.location.pathname === link
      ? window.location.pathname + window.location.search
      : link;
  return (
    <Link className="link-decoration" to={url}>
      <div
        className={`sidebar-list ${isActiveMenus}`}
        onClick={() => handleClassActive(sidebarName)}
      >
        <div>
          <h6 className="font-size-sidebar-name">{sidebarName}</h6>
        </div>
      </div>
    </Link>
  );
};

export default SidebarSubmenuInactive;
