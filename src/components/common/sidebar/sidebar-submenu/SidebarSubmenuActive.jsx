import React from "react";
import { Link } from "react-router-dom";

const SidebarSubmenuActive = ({
  sidebar,
  isActiveMenu,
  isActiveSubMenu,
  handleClassActive
}) => {
  const { sidebarName, subMenu } = sidebar;
  const isActiveMenus = isActiveMenu(sidebarName);
  const isActiveSubMenus = isActiveSubMenu(sidebarName);

  const renderSubMenu = () =>
    subMenu.map((menu) => (
      <Link className="link-decoration" to={menu.link} key={menu.id}>
        <div className="dropdown-list sidebar-list">
          <h6 className="font-size-sidebar-name">{menu.sidebarName}</h6>
        </div>
      </Link>
    ));

  return (
    <React.Fragment>
      <button
        className={`dropdown-btn sidebar-list ${isActiveMenus}`}
        onClick={() => handleClassActive(sidebarName)}
      >
        <h6 className="font-size-sidebar-name">{sidebarName}</h6>
        <i className="fa fa-caret-left" />
      </button>
      <div className={`dropdown-container ${isActiveSubMenus}`}>
        {renderSubMenu()}
      </div>
    </React.Fragment>
  );
};

export default SidebarSubmenuActive;
