import React from "react";
const Input = ({
  input,
  classInput,
  classLabel,
  id,
  label,
  placeholder,
  type,
  value,
  meta: { touched, error }
}) => {
  return (
    <React.Fragment>
      <label className={classLabel} htmlFor={id}>
        {label}
      </label>
      <input
        {...input}
        className={classInput}
        type={type}
        id={id}
        placeholder={placeholder}
      />
      {touched && error && <div className="error-message">*{error}</div>}
      <br />
    </React.Fragment>
  );
};

export default Input;
