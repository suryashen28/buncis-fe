import React from "react";

import config from "@config";
import { handleImageChange } from "@utils/handle-image-change";
import notFoundImage from "@assets/images/not-found.jpg";

const host = config.app.hostImagePath;

const getUrl = (value, initialValues) => {
  return value === initialValues ? `${host}/${value}` : value.url;
};

const imagePreview = (value, initialValues) => {
  return !!value ? (
    <div className="image image-container">
      <img
        src={getUrl(value, initialValues)}
        onError={(e) => (e.target.src = notFoundImage)}
        width="200px"
        height="200px"
        className="image"
        alt="imageCorporate"
      />
    </div>
  ) : (
    <div className="previewText">Please select an Image for Preview</div>
  );
};

const renderLabel = (value) => {
  return !!!value ? "Choose file" : !!value.file ? value.file.name : value;
};

const UploadImage = ({ initialValues, input }) => {
  return (
    <div>
      <h6 className="m-1">Picture</h6>
      <div className="center">
        <div className="image-preview">
          {imagePreview(input.value, initialValues)}
        </div>
      </div>
      <div className="custom-file">
        <input
          onChange={(e) => handleImageChange(e, input.onChange)}
          type="file"
          className="custom-file-input"
          id="custom-file"
          encType="multipart/form-data"
        />
        <label className="custom-file-label" htmlFor="custom-file">
          {renderLabel(input.value)}
        </label>
      </div>
    </div>
  );
};

export default UploadImage;
