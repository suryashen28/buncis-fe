import React from "react";
import DatePicker from "react-datepicker";

import { parseToString } from "@utils/date";
import { date } from "@utils/form-validation";

import "react-datepicker/dist/react-datepicker.css";

const changeDateFormat = (value) => {
  return (
    !date(parseToString(value)) &&
    new Date(value.getFullYear(), value.getMonth(), value.getDate())
  );
};

const DateSelector = ({ label, input, classInput }) => {
  return (
    <div>
      <label>{label}</label>
      <br />
      <DatePicker
        selected={input.value}
        className={classInput}
        disabledKeyboardNavigation
        placeholderText="DD/MM/YYYY"
        onChange={(date) => input.onChange(changeDateFormat(date))}
        showYearDropdown
        showMonthDropdown
      />
      <br />
      <br />
    </div>
  );
};

export default DateSelector;
