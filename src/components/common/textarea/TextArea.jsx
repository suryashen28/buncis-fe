import React from "react";

const TextArea = ({
  input,
  classInput,
  classLabel,
  label,
  id,
  placeholder,
  meta: { touched, error }
}) => {
  return (
    <React.Fragment>
      <label className={classLabel} htmlFor={id}>
        {label}
      </label>
      <textarea
        {...input}
        className={classInput}
        id={id}
        placeholder={placeholder}
      />
      {touched && error && <div className="text-area-warning">*{error}</div>}
      <br />
    </React.Fragment>
  );
};

export default TextArea;
