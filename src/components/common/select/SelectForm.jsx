import React from "react";
import { Form, reduxForm, Field } from "redux-form";

import Select from "@components/common/select/Select";

const SelectForm = (props) => {
  const FIELD = {
    classInput: "select-scroll",
    name: "filter",
    component: Select,
    ...props
  };
  const renderSelect = () => {
    return <Field {...FIELD} />;
  };

  const handleSelect = (res) => {
    props.onChange(res);
  };

  return (
    <Form className="form-inline" onSubmit={props.handleSubmit(handleSelect)}>
      <div className="search-container">{renderSelect()}</div>
    </Form>
  );
};

export default reduxForm({
  form: "selectForm",
  multipartForm: true,
  enableReinitialize: true
})(SelectForm);
