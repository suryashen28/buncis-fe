import React from "react";

const Select = ({
  classInput,
  options,
  id,
  title,
  label,
  input,
  placeholder,
  initialValues,
  isDisabled
}) => {
  const renderOption = () => {
    return options.map((option) => (
      <option key={option.id} value={option.id}>
        {option[title]}
      </option>
    ));
  };

  const renderInitialValue = () => {
    return !!!initialValues && <option hidden>{placeholder}</option>;
  };

  const getValue = () => {
    return !!input?.value ? input.value : initialValues;
  };

  const onChange = (e) => {
    input.onChange(parseInt(e.target.value));
  };

  return (
    <div className="select-container">
      <label>{label}</label>
      <select
        className={classInput}
        id={id}
        onChange={(e) => onChange(e)}
        disabled={isDisabled}
        value={getValue()}
      >
        {renderInitialValue()}
        {renderOption()}
      </select>
    </div>
  );
};

export default Select;
