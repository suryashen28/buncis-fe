import React from 'react';

const Button = ({onClick,button}) => {
    return ( 
        <button type="button" onClick={()=>onClick()} className={button.className}>{button.label}</button>
     );
}
 
export default Button;