import React from "react";
import { Link } from "react-router-dom";

import { logout } from "@utils/auth.js";

const UserProfileMenu = ({ profileRef, toggleProfileDropdown }) => {
  return (
    <div className="profile-cover" onClick={toggleProfileDropdown}>
      <div className="status-profile-menu bgcolor" ref={profileRef}>
        <div className="options">
          <Link to="#">Edit Profile</Link>
        </div>
        <div className="options">
          <Link to="/login" onClick={logout}>
            Logout
          </Link>
        </div>
      </div>
    </div>
  );
};

export default UserProfileMenu;
