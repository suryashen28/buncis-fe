import React, { Component } from "react";

import UserProfileMenu from "@components/common/navbar/user-profile-menu/UserProfileMenu";
import Button from "@components/common/button/Button";

class Navbar extends Component {
  state = {
    profileVisible: false
  };

  toggleProfileDropdown = (e) => {
    if (this.profileRef && this.profileRef.contains(e.target)) return;
    this.setState({ profileVisible: !this.state.profileVisible });
  };

  render() {
    const { props, state, toggleProfileDropdown } = this;
    const { onToggle } = props;
    const { userProfile, profileVisible } = state;

    const buttonNavbarToggler = {
      className: "navbar-toggler",
      label: <i className="fa fa-bars" />
    };

    const buttonNavbar = {
      className: "btn btn-outline-dark my-2 my-sm-0 btn-border",
      label: <i className="fa fa-bars" />
    };

    return (
      <div className="navbar navbar-expand-lg bg-theme">
        <Button onClick={onToggle} button={buttonNavbarToggler} />
        <div className="collapse navbar-collapse navbar-height">
          <Button onClick={onToggle} button={buttonNavbar} />
        </div>

        <div onClick={toggleProfileDropdown}>
          <div className="container">
            <i
              className="fa fa-cog settings-button"
              aria-hidden="true"
              status={userProfile}
            />
          </div>
        </div>

        {profileVisible && (
          <UserProfileMenu
            profileRef={(n) => (this.profileRef = n)}
            toggleProfileDropdown={toggleProfileDropdown}
          />
        )}
      </div>
    );
  }
}

export default Navbar;
