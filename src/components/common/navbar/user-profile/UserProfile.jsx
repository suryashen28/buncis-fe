import React from "react";

import userImage from "@assets/images/users.jpg";

const UserProfile = ({ onClick, url }) => {
  const getUsername = () => localStorage.getItem("name");

  const getSubdivision = () => localStorage.getItem("subdivision");

  return (
    <div className="sidebar-profile-container" onClick={onClick}>
      <div className="sidebar-profile">
        <img className="sidebar-profile-image" src={userImage} alt="users" />
      </div>
      <div>
        <div className="username">{getUsername()}</div>
        <div className="profile-role">{getSubdivision()}</div>
      </div>
    </div>
  );
};

export default UserProfile;
