import React from "react";
import { Form, reduxForm, Field } from "redux-form";

import Input from "@components/common/input/Input";

const Search = (props) => {
  const FIELD = {
    classInput: "search-form",
    name: "search",
    component: Input,
    type: "search",
    placeholder: "Search"
  };

  const renderSearchBar = () => {
    return <Field {...FIELD} />;
  };

  const handleSearch = (res) => {
    props.onSearchSubmit(res);
  };

  return (
    <Form className="form-inline" onSubmit={props.handleSubmit(handleSearch)}>
      <div className="search-container">
        {renderSearchBar()}
        <button type="submit">
          <i className="fa fa-search" />
        </button>
      </div>
    </Form>
  );
};

export default reduxForm({
  form: "search",
  multipartForm: true,
  enableReinitialize: true
})(Search);
