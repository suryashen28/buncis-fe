import React, { Fragment } from "react";
import queryString from "query-string";

import PaginationLoading from "@components/loading/PaginationLoading";

const Pagination = ({
  currentPage,
  numberOfPages,
  history,
  onChange,
  currentPageParams,
  isLoading
}) => {
  const isValidIncrementPage = currentPage + 1 <= numberOfPages;
  const isValidDecrementPage = currentPage - 1 > 0;

  const incrementPage = () => {
    isValidIncrementPage && onChangePages(currentPage + 1);
  };

  const onChangePages = (currentPage) => {
    const pageParams = { ...currentPageParams, page: currentPage };
    history.push({ search: queryString.stringify(pageParams) });
    onChange(currentPage);
  };

  const decrementPage = () => {
    isValidDecrementPage && onChangePages(currentPage - 1);
  };

  const incrementPageStyle = isValidIncrementPage && "bg-theme";
  const decrementPageStyle = isValidDecrementPage && "bg-theme";

  const renderPageIndex = () => {
    return isLoading ? (
      <Fragment>
        <div className={`fa fa-angle-left`} id="button-prev" />
        <PaginationLoading />
        <div className={`fa fa-angle-right`} id="button-prev" />
      </Fragment>
    ) : (
      <Fragment>
        <div
          className={`fa fa-angle-left ${decrementPageStyle}`}
          id="button-prev"
          onClick={() => decrementPage()}
        />
        <span className="page-current">{currentPage}</span> /
        <span className="page-max">{numberOfPages}</span>
        <div
          className={`fa fa-angle-right ${incrementPageStyle}`}
          id="button-next"
          onClick={() => incrementPage()}
        />
      </Fragment>
    );
  };

  return <div className="pagination">{renderPageIndex()}</div>;
};

export default Pagination;
