import React from "react";

const Dropdown = ({ content, handleSubmit, name }) => {
  const renderDropdownContent = () => {
    return content.map((data) => (
      <div
        className={`${name}`}
        key={data.id}
        onClick={() => handleSubmit(data)}
      >
        {data.name}
      </div>
    ));
  };
  return <div>{renderDropdownContent()}</div>;
};

export default Dropdown;
