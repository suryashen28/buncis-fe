import React from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip
} from "recharts";

const ChartArea = ({ data }) => {
  return (
    <div className="chart">
      <AreaChart
        width={900}
        height={310}
        data={data}
        margin={{ top: 10, right: 10, left: 20, bottom: 0 }}
      >
        <defs>
          <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
            <stop offset="25%" stopColor="#F79C3A" stopOpacity={0.8} />
            <stop offset="95%" stopColor="#fac8a2" stopOpacity={0.2} />
          </linearGradient>
        </defs>
        <XAxis dataKey="name" />
        <YAxis />
        <CartesianGrid strokeDasharray="2 2" />
        <Tooltip />
        <Area
          type="monotone"
          dataKey="total"
          stroke=" "
          fillOpacity={1}
          fill="url(#colorPv)"
        />
      </AreaChart>
    </div>
  );
};

export default ChartArea;
