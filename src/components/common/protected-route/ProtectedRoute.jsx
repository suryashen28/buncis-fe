import React, { useState, useEffect } from "react";
import { Route, useHistory } from "react-router-dom";
import { connect } from "react-redux";

import ErrorPage from "@pages/error-page/ErrorPage";
import api from "@api/controllers/auth";

const ProtectedRoute = ({ component: Component, render, ...rest }) => {
  const history = useHistory();
  const [httpStatus, setHttpStatus] = useState();
  useEffect(() => {
    const verifyUser = async () => {
      await api
        .verifyUser()
        .then((httpResponseCode) => setHttpStatus(httpResponseCode));
    };
    verifyUser();
  }, [httpStatus]);
  return (
    <Route
      {...rest}
      render={(props) =>
        httpStatus !== undefined &&
        (httpStatus === 200 ? (
          Component ? (
            <Component {...props} />
          ) : (
            render(props)
          )
        ) : (
          <ErrorPage
            status={httpStatus}
            redirectTo={"/login"}
            history={history}
          />
        ))
      }
    />
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user
  };
};
export default connect(mapStateToProps)(ProtectedRoute);
