import React, { lazy } from "react";

const TableRow = lazy(() =>
  import("@components/common/table/table-row/TableRow")
);

const Table = ({ columns, data }) => {
  const renderRow = (data, columns) => (
    <tbody>
      {data.map((data) => (
        <TableRow key={data.id} columns={columns} data={data} />
      ))}
    </tbody>
  );

  const renderHeader = (columns) =>
    columns.map((column, index) => (
      <th className="table-header-item" key={index}>
        {column.label}
      </th>
    ));

  const renderTable = (data, columns) => {
    return (
      <table className="table table-hover">
        <thead>
          <tr className="table-header">{renderHeader(columns)}</tr>
        </thead>
        {renderRow(data, columns)}
      </table>
    );
  };

  return <div>{renderTable(data, columns)}</div>;
};

export default Table;
