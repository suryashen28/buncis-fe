import React from "react";

const TableRow = ({ columns, data }) => {
  const getChildrenObjectValue = (data, column) =>
    data[column.path][column.subPath]
      ? data[column.path][column.subPath]
      : data[column.path];

  const renderCell = (data, column) =>
    column.content
      ? column.content(data)
      : getChildrenObjectValue(data, column);
  return (
    <React.Fragment>
      <tr key={data.id}>
        {columns.map((column, index) => (
          <th key={index}>{renderCell(data, column)}</th>
        ))}
      </tr>
    </React.Fragment>
  );
};

export default TableRow;
