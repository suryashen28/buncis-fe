import React from "react";
import ReactFocusTrap from "react-focus-trap";

const ModalContent = ({ content, onClose }) => {
  return (
    <ReactFocusTrap
      focusTrapOptions={{ onDeactivate: onClose }}
      className="modal-cover"
    >
      <div className="modal-style">
        <button
          className="fa fa-times modal-close close btn btn-none"
          aria-hidden="true"
          onClick={() => {
            onClose();
          }}
        />
        {content}
      </div>
    </ReactFocusTrap>
  );
};

export default ModalContent;
