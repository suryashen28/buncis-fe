import React, { Component } from "react";
import ModalContent from "@components/common/modal/ModalContent";

class Modal extends Component {
  state = { isOpen: false };

  toggleModal = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    const { isOpen } = this.state;
    const { modalActions, modalContent } = this.props;
    return (
      <div>
        <div onClick={this.toggleModal}>{modalActions}</div>
        {isOpen && (
          <div>
            <ModalContent onClose={this.toggleModal} content={modalContent} />
          </div>
        )}
      </div>
    );
  }
}

export default Modal;
