import React from "react";
import ContentLoader from "react-content-loader";

const DropdownLoading = () => {
  return (
    <ContentLoader height={300} width={1000} backgroundColor="gainsboro">
      <rect x="15" y="15" rx="4" ry="4" width="260" height="10" />
      <rect x="300" y="15" rx="3" ry="3" width="260" height="10" />
      <rect x="295" y="15" rx="3" ry="3" width="180" height="10" />
      <rect x="15" y="50" rx="3" ry="3" width="180" height="10" />
      <rect x="230" y="50" rx="3" ry="3" width="120" height="10" />
      <rect x="500" y="50" rx="3" ry="3" width="400" height="10" />
      <rect x="15" y="90" rx="3" ry="3" width="260" height="10" />
      <rect x="300" y="90" rx="3" ry="3" width="240" height="10" />
      <rect x="600" y="90" rx="3" ry="3" width="190" height="10" />
      <rect x="160" y="130" rx="3" ry="3" width="450" height="10" />
      <rect x="500" y="130" rx="3" ry="3" width="260" height="10" />
      <rect x="70" y="170" rx="3" ry="3" width="100" height="10" />
      <rect x="200" y="170" rx="3" ry="3" width="450" height="10" />
      <rect x="600" y="170" rx="3" ry="3" width="260" height="10" />
    </ContentLoader>
  );
};

export default DropdownLoading;
