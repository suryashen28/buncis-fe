import React from "react";
import ContentLoader from "react-content-loader";

const CardLoading = () => {
  return (
    <div className="loading-card-container">
      <ContentLoader>
        <rect x="0" y="20" rx="4" ry="4" width="200" height="13" />
        <rect x="0" y="60" rx="3" ry="3" width="100" height="13" />
        <rect x="0" y="90" rx="4" ry="4" width="200" height="13" />
        <rect x="0" y="120" rx="3" ry="3" width="100" height="13" />
        <rect x="220" y="30" rx="5" ry="5" width="70" height="70" />
      </ContentLoader>
    </div>
  );
};

export default CardLoading;
