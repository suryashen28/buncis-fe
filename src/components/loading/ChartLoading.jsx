import React from "react";

import Spinner from "@components/common/spinner/Spinner";

const ChartLoading = () => {
  return (
    <div className="chart-loading-container">
      <Spinner />
    </div>
  );
};

export default ChartLoading;
