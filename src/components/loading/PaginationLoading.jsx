import React from "react";
import ContentLoader from "react-content-loader";

const PaginationLoading = () => {
  return (
    <ContentLoader backgroundColor={"grey"} height={40} width={100}>
      <rect x={10} y={5} rx={4} ry={4} width={30} height={30} />
      <rect x={60} y={5} rx={3} ry={3} width={30} height={30} />
    </ContentLoader>
  );
};

export default PaginationLoading;
