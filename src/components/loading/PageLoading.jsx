import React from "react";

import Spinner from "@components/common/spinner/Spinner";

const PageLoading = () => {
  return (
    <div className="loading-page-container">
      <Spinner />
    </div>
  );
};

export default PageLoading;
