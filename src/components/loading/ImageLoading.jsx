import React from "react";
import ContentLoader from "react-content-loader";

const ImageLoading = () => {
  return (
    <ContentLoader className="image-loading-container">
      <rect className="image-loading" x="-20" rx="10" ry="10" />
    </ContentLoader>
  );
};

export default ImageLoading;
