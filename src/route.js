const routeList = {
  dashboard: { path: "/pr/dashboard" },

  corporateForm: {
    path: { add: "/pr/corporate/add", edit: "/pr/corporate/:id/edit" }
  },
  corporate: { path: "/pr/corporate" },

  corporateContactForm: {
    path: {
      add: "/pr/corporate-contact/add",
      edit: "/pr/corporate-contact/:id/edit"
    }
  },
  corporateContact: {
    path: "/pr/corporate-contact"
  },

  collaborationForm: {
    path: { add: "/pr/collaboration/add", edit: "/pr/collaboration/:id/edit" }
  },
  collaboration: { path: "/pr/collaboration" },

  collaborationTypeForm: {
    path: {
      add: "/pr/collaboration-type/add",
      edit: "/pr/collaboration-type/:id/edit"
    }
  },
  collaborationType: {
    path: "/pr/collaboration-type"
  }
};

export default routeList;
