const developmentURL = "http://buncis-fe:3000";
const productionURL = "http://buncis-laravel:8000";

const hostURL =
  process.env.NODE_ENV === "development" ? developmentURL : productionURL;

const hostImagePath =
  process.env.NODE_ENV === "development"
    ? `${developmentURL}/assets/images`
    : `${hostURL}/storage/images`;

export default {
  host: hostURL,
  currentHost: developmentURL,
  currentHostImagePath: `${developmentURL}/assets/images`,
  hostImagePath: hostImagePath
};
