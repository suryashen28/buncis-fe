import PR from "@config/urls/PR/index";
import EEO from "@config/urls/EEO/index";
import auth from "@config/urls/auth/index";

export default {
  PR,
  EEO,
  auth
};
