export default {
  getCollaborationType: `/api/pr/collaboration-types`,
  createCollaborationType: `/api/pr/collaboration-type`,
  getCollaborationTypeByID: (id) => `/api/pr/collaboration-type/${id}`,
  updateCollaborationType: (id) => `/api/pr/collaboration-type/${id}`,
  deleteCollaborationType: (id) => `/api/pr/collaboration-type/${id}`
};
