export default {
  getCollaborations: `/api/pr/collaborations`,
  createCollaboration: `/api/pr/collaboration`,
  getCollaborationByID: (id) => `/api/pr/collaboration/${id}`,
  updateCollaboration: (id) => `/api/pr/collaboration/${id}`,
  deleteCollaboration: (id) => `/api/pr/collaboration/${id}`
};
