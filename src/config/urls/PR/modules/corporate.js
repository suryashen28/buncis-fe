export default {
  getCorporates: `/api/pr/corporates`,
  createCorporate: `/api/pr/corporate`,
  getCorporateByID: (id) => `/api/pr/corporate/${id}`,
  updateCorporate: (id) => `/api/pr/corporate/${id}`,
  deleteCorporate: (id) => `/api/pr/corporate/${id}`
};
