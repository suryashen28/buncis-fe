export default {
  getCorporateContacts: `/api/pr/corporate-contacts`,
  createCorporateContact: `/api/pr/corporate-contact`,
  updateCorporateContact: (id) => `/api/pr/corporate-contact/${id}`,
  getCorporateContactByID: (id) => `/api/pr/corporate-contact/${id}`,
  deleteCorporateContact: (id) => `/api/pr/corporate-contact/${id}`
};
