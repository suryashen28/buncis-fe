export default {
  getCollaborationStatistic: `/api/pr/collaborations-statistic`,
  getContactStatistic: `/api/pr/corporate-contacts-statistic`,
  getProfitStatistic: `/api/pr/collaborations/profit-statistic`
};
