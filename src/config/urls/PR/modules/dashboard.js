export default {
  getCountCorporateContact: `/api/pr/corporate-contacts-count`,
  getCountCollaborationThisYear: `/api/pr/collaboration-year-count`,
  getRecentCollaboration: `/api/pr/collaboration-recent`
};
