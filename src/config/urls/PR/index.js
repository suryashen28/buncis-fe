import corporate from "@config/urls/PR/modules/corporate";
import collaborationType from "@config/urls/PR/modules/collaboration-type";
import collaborationStatistic from "@config/urls/PR/modules/collaboration-statistic";
import dashboardPR from "@config/urls/PR/modules/dashboard";
import collaboration from "@config/urls/PR/modules/collaboration";
import corporateContact from "@config/urls/PR/modules/corporate-contact";

export default {
  corporate,
  collaborationType,
  collaborationStatistic,
  dashboardPR,
  collaboration,
  corporateContact
};
