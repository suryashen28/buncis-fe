export default {
  login: `api/auth/login`,
  verify: `api/auth/verify`
};
