import event from "@config/urls/EEO/modules/event";
import partnerType from "@config/urls/EEO/modules/partner-type";
import partner from "@config/urls/EEO/modules/partner";
import dashboardEEO from "@config/urls/EEO/modules/dashboard";
import eventStatistic from "@config/urls/EEO/modules/event-statistic";
import eventDetail from "@config/urls/EEO/modules/event-detail";
import participant from "@config/urls/EEO/modules/participant";
import role from "@config/urls/EEO/modules/role";
import major from "@config/urls/EEO/modules/major";

export default {
  event,
  partnerType,
  partner,
  dashboardEEO,
  eventStatistic,
  eventDetail,
  participant,
  role,
  major,
};
