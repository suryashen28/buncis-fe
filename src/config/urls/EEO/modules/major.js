export default {
  getMajors: `api/eeo/majors`,
  createMajor: `api/eeo/major`,
  deleteMajor: (id) => `api/eeo/major/${id}`,
  getMajorById: (id) => `api/eeo/major/${id}`,
  updateMajor: (id) => `api/eeo/major/${id}`
};
