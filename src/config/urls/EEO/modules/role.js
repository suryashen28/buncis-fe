export default {
  getRoles: `/api/eeo/role`,
  createRole: `api/eeo/role`,
  deleteRole: (id) => `api/eeo/role/${id}`,
  updateRole: (id) => `api/eeo/role/${id}`
};
