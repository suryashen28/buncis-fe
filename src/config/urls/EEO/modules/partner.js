export default {
  getPartners: `/api/eeo/partners`,
  createPartner: `api/eeo/partner`,
  getPartnerById: (id) => `api/eeo/partner/${id}`,
  updatePartner: (id) => `api/eeo/partner/${id}`,
  deletePartner: (id) => `api/eeo/partner/${id}`
};
