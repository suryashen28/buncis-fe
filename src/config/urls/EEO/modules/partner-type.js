export default {
  getPartnerTypes: `/api/eeo/partner-types`,
  createPartnerType: `/api/eeo/partner-type`,
  getPartnerTypeById: (id) => `/api/eeo/partner-type/${id}`,
  updatePartnerType: (id) => `/api/eeo/partner-type/${id}`,
  deletePartnerType: (id) => `/api/eeo/partner-type/${id}`
};
