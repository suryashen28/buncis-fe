export default {
  updateParticipant: (eventId, id) =>
    `api/eeo/event/${eventId}/participant/${id}`,
  createParticipant: (eventId) => `api/eeo/event/${eventId}/participant/`,
  getParticipantById: (eventId, id) =>
    `api/eeo/event/${eventId}/participant/${id}`,
  getParticipantsByPartnerTypeId: (eventId, partnerTypeId) =>
    `api/eeo/event/${eventId}/participant/partnerType/${partnerTypeId}`,
  deleteParticipant: (eventId, id) =>
    `api/eeo/event/${eventId}/participant/${id}`,
  createParticipants: (eventId, partnerTypeId) =>
    `api/eeo/event/${eventId}/participant/partnerType/${partnerTypeId}`
};
