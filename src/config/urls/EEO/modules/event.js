export default {
  getEvents: `/api/eeo/events`,
  createEvent: `/api/eeo/event`,
  getEventById: (id) => `/api/eeo/event/${id}`,
  updateEvent: (id) => `/api/eeo/event/${id}`,
  deleteEvent: (id) => `/api/eeo/event/${id}`
};
