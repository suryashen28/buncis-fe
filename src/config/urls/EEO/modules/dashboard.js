export default {
  getTopUniversityAttendee: `api/eeo/universities-top`,
  getTopMajorAttendee: `api/eeo/majors-top`,
  getRecentEvents: `api/eeo/events-top`,
  getCountPartner: `api/eeo/partner-count`
};
