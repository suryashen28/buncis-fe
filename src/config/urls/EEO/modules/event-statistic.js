export default {
  getEventParticipantStatistic: `api/eeo/events/participants-count`,
  getEventProfitStatistic: `api/eeo/events/profit-total`
};
