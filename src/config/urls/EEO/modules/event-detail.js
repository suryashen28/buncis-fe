export default {
  getEventDetailNote: (id) => `api/eeo/event/${id}/notes`,
  createEventDetailNote: (id) => `api/eeo/event/${id}/note`,
  updateEventDetailNote: (id, noteId) => `api/eeo/event/${id}/note/${noteId}`,
  deleteEventDetailNote: (id, noteId) => `api/eeo/event/${id}/note/${noteId}`,
  getEventDetailDocumentation: (id) => `api/eeo/event/${id}/documentations`,
  createEventDetailDocumentation: (id) => `api/eeo/event/${id}/documentation`,
  updateEventDetailDocumentation: (id, documentationId) =>
    `api/eeo/event/${id}/documentation/${documentationId}`
};
