import app from "@config/app";
import urls from "@config/urls";
import environment from "@config/environment";

export default {
  app,
  urls,
  environment
};
