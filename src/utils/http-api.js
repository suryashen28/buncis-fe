import config from "@src/config";

const tokenType = localStorage.getItem("tokenType");
const accessToken = localStorage.getItem("accessToken");

const isDevelopment = process.env.NODE_ENV === config.environment.DEVELOPMENT;

const getDataFromSuccessResult = (data) => {
  return isDevelopment ? data.data : data;
};

export const getAPIData = (response) => {
  const result = isDevelopment ? response : response.data;
  return result.status === 200 || result.status === 201
    ? getDataFromSuccessResult(result.data)
    : (() => {
        throw new Error(result.message);
      })();
};

export const getAccessToken = () => {
  return `${tokenType} ${accessToken}`;
};
