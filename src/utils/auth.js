export const login = (user) => {
  Object.keys(user).forEach((keys) => {
    localStorage.setItem(keys, user[keys]);
  });
};

export const logout = () => localStorage.clear();

export const getUser = () => {
  return { ...localStorage };
};
