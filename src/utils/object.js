export const addRequiredAttributeToObject = (data, requiredAttribute) => {
  return requiredAttribute.reduce(
    (resultObject, x) => {
      !!data[x.nestedAttribute] &&
        (resultObject[x.requiredAttribute] = data[x.nestedAttribute]["id"]);
      return resultObject;
    },
    { ...data }
  );
};
