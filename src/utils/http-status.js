const httpStatusList = {
  200: {
    status: 200,
    message: "Success"
  },
  400: {
    status: 400,
    message: "Bad Request"
  },
  401: {
    status: 401,
    message: "Unauthorized",
    response:
      "Ouch! You have attempted to access a page which you're not authorized Please login first"
  },
  403: {
    status: 403,
    message: "Forbidden Access",
    response:
      "Whoa! You just don't have the permission to access this page Please go back or return to our homepage"
  },
  404: {
    status: 404,
    message: "Page Not Found",
    response:
      "Oops! It's look like the page you're trying can't be found Please go back or return to our homepage"
  },
  500: {
    status: 500,
    message: "Internal Server Error",
    response:
      "Oops! It's look like our internal server has done it again Please try to contact our Research and Development team for further information"
  }
};

export default httpStatusList;
