export const getThisYear = () => new Date().getFullYear();
export const parseToString = (date) =>
  `${date.getFullYear()}-${(`0` + (date.getMonth() + 1)).slice(-2)}-${(
    `0` + date.getDate()
  ).slice(-2)}`;

export const getTodayDate = () => {
  const time = new Date();
  return new Date(time.getFullYear(), time.getMonth(), time.getDate());
};

export const getExpireTokenTime = () => {
  const time = new Date();
  time.setHours(time.getHours() + 2);
  return time;
};

export const getListOfPassedYears = (numberOfPassedYears) => {
  const listOfPassedYears = [];
  const thisYear = getThisYear();
  while (numberOfPassedYears > 0) {
    numberOfPassedYears -= 1;
    const year = {
      id: thisYear - numberOfPassedYears
    };
    listOfPassedYears.push(year);
  }
  return listOfPassedYears.reverse();
};
