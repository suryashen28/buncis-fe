export const handleImageChange = (e, onSuccess) => {
  e.preventDefault();
  const reader = new FileReader();
  const file = e.target.files[0];
  reader.onloadend = () => {
    const result = {
      file: file,
      url: reader.result
    };
    onSuccess && onSuccess(result);
  };
  reader.readAsDataURL(file);
};
