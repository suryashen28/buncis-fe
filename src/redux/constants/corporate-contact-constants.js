export default {
  GET_CORPORATE_CONTACTS: "GET_CORPORATE_CONTACTS",
  GET_ALL_CORPORATE_CONTACTS: "GET_ALL_CORPORATE_CONTACTS",
  GET_CORPORATE_CONTACT_BY_ID: "GET_CORPORATE_CONTACT_BY_ID",
  CREATE_CORPORATE_CONTACT: "CREATE_CORPORATE_CONTACT",
  UPDATE_CORPORATE_CONTACT: "UPDATE_CORPORATE_CONTACT",
  DELETE_CORPORATE_CONTACT: "DELETE_CORPORATE_CONTACT",
  RESET_LOADING: "RESET_LOADING",
  RESET_DROPDOWN_LOADING: "RESET_DROPDOWN_LOADING",
  RESET_CORPORATE_CONTACT_FORM: "RESET_CORPORATE_CONTACT_FORM"
};
