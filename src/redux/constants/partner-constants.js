export default {
  GET_PARTNERS: "GET_PARTNERS",
  GET_PARTNER_BY_ID: "GET_PARTNER_BY_ID",
  CREATE_PARTNER: "CREATE_PARTNER",
  UPDATE_PARTNER: "UPDATE_PARTNER",
  DELETE_PARTNER: "DELETE_PARTNER",
  RESET_PARTNER_FORM: "RESET_PARTNER_FORM"
};
