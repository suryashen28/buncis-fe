export default {
  GET_TOP_UNIVERSITY_ATTENDEE: "GET_TOP_UNIVERSITY_ATTENDEE",
  GET_TOP_MAJOR_ATTENDEE: "GET_TOP_MAJOR_ATTENDEE",
  GET_RECENT_EVENTS: "GET_RECENT_EVENTS",
  GET_COUNT_PARTNER: "GET_COUNT_PARTNER"
};
