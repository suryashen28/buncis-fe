export default {
  GET_COLLABORATION_STATISTIC: "GET_COLLABORATION_STATISTIC",
  GET_CONTACT_STATISTIC: "GET_CONTACT_STATISTIC",
  GET_PROFIT_STATISTIC: "GET_PROFIT_STATISTIC",
  RESET_DASHBOARD_CARD_LOADING: "RESET_DASHBOARD_CARD_LOADING",
  RESET_LOADING: "RESET_LOADING"
};
