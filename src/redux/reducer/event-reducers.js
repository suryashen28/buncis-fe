import ActionType from "@redux/constants/event-constants";

const eventReducers = (
  state = {
    event: [],
    loading: false,
    error: null,
    form: {},
    eventOptions: []
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_EVENTS:
      return {
        ...state,
        loading: false,
        event: action.payload.data
      };
    case ActionType.GET_ALL_EVENTS:
      return {
        ...state,
        eventOptions: action.payload.data
      };
    case ActionType.DELETE_EVENT: {
      const event = state.event.filter((event) => event.id !== action.payload);
      return {
        ...state,
        event
      };
    }
    case ActionType.UPDATE_EVENT:
      return {
        ...state
      };
    case ActionType.CREATE_EVENT: {
      const event = [...state.event, action.payload.data];
      return {
        ...state,
        loading: false,
        event
      };
    }
    case ActionType.GET_EVENT_BY_ID:
      return {
        ...state,
        form: action.payload.data
      };
    default:
      return state;
  }
};

export default eventReducers;
