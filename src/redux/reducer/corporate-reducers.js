import ActionType from "@redux/constants/corporate-constants";

const corporateReducers = (
  state = {
    corporate: [],
    page: { page: 1 },
    isLoading: true,
    error: null,
    form: {},
    corporateOptions: []
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_CORPORATES:
      return {
        ...state,
        page: action.payload.page,
        isLoading: false,
        corporate: action.payload.data
      };
    case ActionType.GET_ALL_CORPORATES:
      return {
        ...state,
        isDropdownLoading: false,
        corporateOptions: action.payload.data
      };
    case ActionType.CREATE_CORPORATE: {
      const corporate = [...state.corporate, action.payload.data];
      return {
        ...state,
        corporate
      };
    }
    case ActionType.HANDLE_ERROR:
      return {
        ...state,
        error: action.payload.error,
        corporate: []
      };
    case ActionType.GET_CORPORATE_BY_ID:
      return {
        ...state,
        form: action.payload.data
      };

    case ActionType.UPDATE_CORPORATE:
      return {
        ...state
      };

    case ActionType.DELETE_CORPORATE: {
      const corporate = state.corporate.filter(
        (corporate) => corporate.id !== action.payload
      );
      return {
        ...state,
        corporate
      };
    }

    case ActionType.RESET_CORPORATE_FORM: {
      return {
        ...state,
        form: {}
      };
    }

    case ActionType.RESET_LOADING: {
      return {
        ...state,
        isLoading: true
      };
    }

    case ActionType.RESET_DROPDOWN_LOADING: {
      return {
        ...state,
        isDropdownLoading: true
      };
    }
    default:
      return state;
  }
};
export default corporateReducers;
