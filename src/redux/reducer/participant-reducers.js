import ActionType from "@redux/constants/participant-constants";

const participantReducers = (
  state = {
    participant: [],
    loading: false,
    error: null,
    form: {}
  },
  action
) => {
  switch (action.type) {
    case ActionType.UPDATE_PARTICIPANT:
      return {
        ...state
      };
    case ActionType.CREATE_PARTICIPANT: {
      const participant = [...state.participant, action.payload.data];
      return {
        ...state,
        loading: false,
        participant
      };
    }
    case ActionType.GET_PARTICIPANT_BY_ID:
      return {
        ...state,
        form: action.payload.data
      };
    case ActionType.GET_PARTICIPANT_BY_PARTNER_TYPE_ID:
      return {
        ...state,
        form: action.payload.data
      };
    case ActionType.DELETE_PARTICIPANT: {
      const participant = state.participant.filter(
        (participant) => participant.id !== action.payload
      );
      return {
        ...state,
        participant
      };
    }
    case ActionType.CREATE_PARTICIPANTS: {
      const participant = [...state.participant, action.payload.data];
      return {
        ...state,
        loading: false,
        participant
      };
    }
    default:
      return state;
  }
};
export default participantReducers;
