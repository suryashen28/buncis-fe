import ActionType from "@redux/constants/dashboard-eeo-constants";

const dashboardEEOReducers = (
  state = {
    countPartner: null,
    topAttendee: [],
    recentEvents: [],
    partnerCount: null,
    loading: true
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_TOP_UNIVERSITY_ATTENDEE:
      return {
        ...state,
        loading: false,
        topAttendee: action.payload.data
      };
    case ActionType.GET_TOP_MAJOR_ATTENDEE:
      return {
        ...state,
        loading: false,
        topAttendee: action.payload.data
      };
    case ActionType.GET_RECENT_EVENTS:
      return {
        ...state,
        loading: false,
        recentEvents: action.payload.data
      };
    case ActionType.GET_COUNT_PARTNER:
      return {
        ...state,
        loading: false,
        countPartner: action.payload.data
      };
    default:
      return state;
  }
};

export default dashboardEEOReducers;
