import ActionType from "@redux/constants/event-detail-constants";

const eventDetailReducers = (
  state = {
    eventDetailNote: [],
    eventDetailDocumentation: [],
    loading: false,
    error: null
  },
  action
) => {
  switch (action.type) {
    case ActionType.CREATE_EVENT_DETAIL_NOTE: {
      const eventDetailNote = [...state.eventDetailNote, action.payload.data];
      return {
        ...state,
        loading: false,
        eventDetailNote
      };
    }
    case ActionType.GET_EVENT_DETAIL_NOTE:
      return {
        ...state,
        loading: false,
        eventDetailNote: action.payload.data
      };
    case ActionType.UPDATE_EVENT_DETAIL_NOTE:
      return {
        ...state
      };
    case ActionType.DELETE_EVENT_DETAIL_NOTE: {
      const eventDetailNote = state.eventDetailNote.filter(
        (eventDetailNote) => eventDetailNote.id !== action.payload
      );
      return {
        ...state,
        eventDetailNote
      };
    }
    case ActionType.CREATE_EVENT_DETAIL_DOCUMENTATION: {
      const eventDetailDocumentation = [
        ...state.eventDetailDocumentation,
        action.payload.data
      ];
      return {
        ...state,
        loading: false,
        eventDetailDocumentation
      };
    }
    case ActionType.GET_EVENT_DETAIL_DOCUMENTATION:
      return {
        ...state,
        loading: false,
        eventDetailDocumentation: action.payload.data
      };
    case ActionType.UPDATE_EVENT_DETAIL_DOCUMENTATION:
      return {
        ...state
      };
    default:
      return state;
  }
};

export default eventDetailReducers;
