import ActionType from "@redux/constants/partner-type-constants";

const partnerTypeReducers = (
  state = {
    partnerType: [],
    loading: false,
    error: null,
    form: {},
    partnerTypeOptions: []
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_PARTNER_TYPES:
      return {
        ...state,
        loading: false,
        partnerType: action.payload.data
      };

    case ActionType.GET_ALL_PARTNER_TYPES:
      return {
        ...state,
        partnerTypeOptions: action.payload.data
      };

    case ActionType.CREATE_PARTNER_TYPE:
      const partnerTypes = [...state.partnerType, action.payload.data];
      return {
        ...state,
        partnerTypes
      };

    case ActionType.GET_PARTNER_TYPE_BY_ID:
      return {
        form: action.payload.data
      };

    case ActionType.UPDATE_PARTNER_TYPE:
      return {
        ...state
      };

    case ActionType.DELETE_PARTNER_TYPE:
      const partnerType = state.partnerType.filter(
        (partnerType) => partnerType.id !== action.payload
      );
      return { ...state, partnerType };

    case ActionType.RESET_PARTNER_TYPE_FORM: {
      return {
        ...state,
        form: {}
      };
    }

    default:
      return state;
  }
};

export default partnerTypeReducers;
