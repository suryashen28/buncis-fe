import ActionType from "@redux/constants/major-constants";

const majorReducers = (
  state = {
    major: [],
    loading: false,
    error: null,
    form: {}
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_MAJORS:
      return {
        ...state,
        loading: false,
        major: action.payload.data
      };
    case ActionType.GET_MAJOR_BY_ID:
      return {
        ...state,
        form: action.payload.data
      };
    case ActionType.CREATE_MAJOR: {
      const major = [...state.major, action.payload.data];
      return {
        ...state,
        loading: false,
        major
      };
    }
    case ActionType.UPDATE_MAJOR:
      return {
        ...state
      };
    case ActionType.DELETE_MAJOR: {
      const major = state.major.filter((major) => major.id !== action.payload);
      return {
        ...state,
        major
      };
    }
    default:
      return state;
  }
};
export default majorReducers;
