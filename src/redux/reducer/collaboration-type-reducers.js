import ActionType from "@redux/constants/collaboration-type-constants";

const collaborationTypeReducers = (
  state = {
    collaborationType: [],
    page: { page: 1 },
    error: null,
    form: {},
    isLoading: true,
    collaborationTypeOptions: []
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_COLLABORATION_TYPES:
      return {
        ...state,
        page: action.payload.page,
        collaborationType: action.payload.data,
        isLoading: false
      };
    case ActionType.GET_ALL_COLLABORATION_TYPES:
      return {
        ...state,
        collaborationTypeOptions: action.payload.data
      };
    case ActionType.CREATE_COLLABORATION_TYPE:
      const collaborationType = [
        ...state.collaborationType,
        action.payload.data
      ];
      return {
        ...state,
        collaborationType
      };

    case ActionType.HANDLE_ERROR:
      return {
        ...state,
        error: action.payload.error,
        collaborationType: []
      };

    case ActionType.GET_COLLABORATION_TYPE_BY_ID:
      return {
        ...state,
        form: action.payload.data
      };

    case ActionType.UPDATE_COLLABORATION_TYPE:
      return {
        ...state
      };

    case ActionType.DELETE_COLLABORATION_TYPE: {
      const collaborationType = state.collaborationType.filter(
        (collaborationType) => collaborationType.id !== action.payload
      );
      return { ...state, collaborationType };
    }

    case ActionType.RESET_COLLABORATION_TYPE_FORM: {
      return {
        ...state,
        form: {}
      };
    }

    case ActionType.RESET_LOADING: {
      return {
        ...state,
        isLoading: true
      };
    }
    default:
      return state;
  }
};

export default collaborationTypeReducers;
