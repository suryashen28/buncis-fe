import ActionType from "@redux/constants/event-constants";

const partnerReducers = (
  state = {
    partner: [],
    loading: false,
    error: null,
    form: {}
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_PARTNERS:
      return {
        ...state,
        loading: false,
        partner: action.payload.data
      };

    case ActionType.CREATE_PARTNER:
      const partner = [...state.partner, action.payload.data];
      return {
        ...state,
        partner: partner
      };

    case ActionType.GET_PARTNER_BY_ID:
      return {
        form: action.payload.data
      };

    case ActionType.UPDATE_PARTNER:
      return {
        ...state
      };

    case ActionType.DELETE_PARTNER:
      const partners = state.partner.filter(
        (partner) => partner.id !== action.payload
      );
      return { ...state, partners };

    case ActionType.RESET_PARTNER_FORM: {
      return {
        ...state,
        form: {}
      };
    }

    default:
      return state;
  }
};

export default partnerReducers;
