import ActionType from "@redux/constants/corporate-contact-constants";
import { addRequiredAttributeToObject } from "@utils/object";

const convertNestedObjectToRequiredAttribute = (data) => {
  const requiredAttribute = [
    { nestedAttribute: "corporate", requiredAttribute: "corporateId" }
  ];
  const nestedObjectWithRequiredAttribute = addRequiredAttributeToObject(
    data,
    requiredAttribute
  );
  const { corporate, ...rest } = nestedObjectWithRequiredAttribute;
  return rest;
};

const corporateContactReducers = (
  state = {
    corporateContact: [],
    isLoading: true,
    isDropdownLoading: true,
    page: { page: 1 },
    error: null,
    form: {},
    corporateContactOptions: []
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_CORPORATE_CONTACTS:
      return {
        ...state,
        page: action.payload.page,
        isLoading: false,
        corporateContact: action.payload.data
      };
    case ActionType.GET_ALL_CORPORATE_CONTACTS:
      return {
        ...state,
        isDropdownLoading: false,
        corporateContactOptions: action.payload.data
      };
    case ActionType.CREATE_CORPORATE_CONTACT: {
      const corporateContact = [...state.corporateContact, action.payload.data];
      return {
        ...state,
        corporateContact
      };
    }

    case ActionType.GET_CORPORATE_CONTACT_BY_ID:
      const formData = convertNestedObjectToRequiredAttribute(
        action.payload.data
      );
      return {
        ...state,
        form: formData
      };

    case ActionType.UPDATE_CORPORATE_CONTACT:
      return {
        ...state
      };

    case ActionType.DELETE_CORPORATE_CONTACT: {
      const corporateContact = state.corporateContact.filter(
        (corporateContact) => corporateContact.id !== action.payload
      );
      return { ...state, corporateContact };
    }

    case ActionType.RESET_CORPORATE_CONTACT_FORM: {
      return {
        ...state,
        form: {}
      };
    }

    case ActionType.RESET_LOADING: {
      return {
        ...state,
        isLoading: true
      };
    }

    case ActionType.RESET_DROPDOWN_LOADING: {
      return {
        ...state,
        isDropdownLoading: true
      };
    }
    default:
      return state;
  }
};

export default corporateContactReducers;
