import ActionType from "@redux/constants/event-statistic-constants";

const eventStatisticReducers = (
  state = {
    eventStatistic: [],
    error: null
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_EVENT_PARTICIPANT_STATISTIC:
      return {
        ...state,
        eventStatistic: action.payload.data
      };
    case ActionType.GET_EVENT_PROFIT_STATISTIC:
      return {
        ...state,
        eventStatistic: action.payload.data
      };
    default:
      return state;
  }
};

export default eventStatisticReducers;
