import ActionType from "@redux/constants/dashboard-pr-constants";

export const recentCollaborationReducers = (
  state = {
    recentCollaboration: [],
    isLoading: true
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_RECENT_COLLABORATION:
      return {
        ...state,
        isLoading: false,
        recentCollaboration: action.payload.data
      };
    default:
      return state;
  }
};

export const dashboardPRCardReducers = (
  state = {
    countCollaborationThisYear: null,
    countCorporateContact: null,
    isLoading: true
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_COUNT_COLLABORATION_THIS_YEAR:
      return {
        ...state,
        isLoading: false,
        countCollaborationThisYear: action.payload.data
      };
    case ActionType.GET_COUNT_CORPORATE_CONTACT:
      return {
        ...state,
        loading: false,
        countCorporateContact: action.payload.data
      };
    default:
      return state;
  }
};
