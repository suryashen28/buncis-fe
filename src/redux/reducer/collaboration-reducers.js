import ActionType from "@redux/constants/collaboration-constants";
import { addRequiredAttributeToObject } from "@utils/object";

const convertNestedObjectToRequiredAttribute = (data) => {
  const requiredAttribute = [
    { nestedAttribute: "corporate", requiredAttribute: "corporateId" },
    {
      nestedAttribute: "collaborationType",
      requiredAttribute: "collaborationTypeId"
    },
    {
      nestedAttribute: "corporateContact",
      requiredAttribute: "corporateContactId"
    },
    { nestedAttribute: "event", requiredAttribute: "eventId" }
  ];
  const nestedObjectWithRequiredAttribute = addRequiredAttributeToObject(
    data,
    requiredAttribute
  );
  const {
    corporate,
    collaborationType,
    corporateContact,
    event,
    ...rest
  } = nestedObjectWithRequiredAttribute;
  return rest;
};

const getFormData = (data) => {
  const convertedData = convertNestedObjectToRequiredAttribute(data);
  const form = { ...convertedData, date: new Date(data.date) };
  return form;
};

const collaborationReducers = (
  state = {
    collaboration: [],
    isLoading: true,
    error: null,
    form: {},
    page: { page: 1 }
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_COLLABORATIONS:
      return {
        ...state,
        isLoading: false,
        page: action.payload.page,
        collaboration: action.payload.data
      };

    case ActionType.CREATE_COLLABORATIONS: {
      const collaboration = [...state.collaborations, action.payload];
      return {
        ...state,
        collaboration
      };
    }

    case ActionType.UPDATE_COLLABORATIONS:
      return {
        ...state
      };

    case ActionType.GET_COLLABORATION_BY_ID:
      const data = action.payload.data;
      const form = getFormData(data);
      return {
        ...state,
        form: form
      };

    case ActionType.DELETE_COLLABORATION: {
      const collaboration = state.collaboration.splice(action.payload);
      return { ...state, collaboration };
    }

    case ActionType.RESET_COLLABORATION_FORM: {
      return {
        ...state,
        form: {}
      };
    }

    case ActionType.RESET_LOADING: {
      return {
        ...state,
        isLoading: true
      };
    }
    default:
      return state;
  }
};

export default collaborationReducers;
