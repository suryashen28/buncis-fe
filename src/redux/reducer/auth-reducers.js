import ActionType from "@redux/constants/auth-constants";

const authReducers = (
  state = {
    user: null,
    form: {}
  },
  action
) => {
  switch (action.type) {
    case ActionType.LOGIN:
      const user = action.payload.data;
      return {
        ...state,
        user: user
      };
    case ActionType.RESET_LOGIN_FORM:
      return {
        ...state,
        form: {}
      };
    default:
      return state;
  }
};
export default authReducers;
