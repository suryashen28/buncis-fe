import ActionType from "@redux/constants/collaboration-statistic-constants";

const collaborationStatisticReducers = (
  state = {
    collaborationStatistic: [],
    isLoading: true,
    error: null
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_COLLABORATION_STATISTIC:
      return {
        ...state,
        isLoading: false,
        collaborationStatistic: action.payload.data
      };
    case ActionType.GET_CONTACT_STATISTIC:
      return {
        ...state,
        isLoading: false,
        collaborationStatistic: action.payload.data
      };
    case ActionType.GET_PROFIT_STATISTIC:
      return {
        ...state,
        isLoading: false,
        collaborationStatistic: action.payload.data
      };
    case ActionType.RESET_LOADING:
      return {
        ...state,
        isLoading: true
      };
    default:
      return state;
  }
};

export default collaborationStatisticReducers;
