import ActionType from "@redux/constants/role-constants";

const roleReducers = (
  state = {
    role: [],
    loading: false,
    error: null,
    form: {}
  },
  action
) => {
  switch (action.type) {
    case ActionType.GET_ROLES:
      return {
        ...state,
        loading: false,
        role: action.payload.data
      };
    case ActionType.DELETE_ROLE: {
      const role = state.role.filter((role) => role.id !== action.payload);
      return { ...state, role };
    }
    case ActionType.CREATE_ROLE: {
      const role = [...state.role, action.payload.data];
      return {
        ...state,
        role: role
      };
    }
    case ActionType.UPDATE_ROLE:
      return {
        ...state
      };
    default:
      return state;
  }
};

export default roleReducers;
