import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { reducer as formReducers } from "redux-form";

import corporateReducers from "@redux/reducer/corporate-reducers";
import authReducers from "@redux/reducer/auth-reducers";
import collaborationTypeReducers from "@redux/reducer/collaboration-type-reducers";
import collaborationStatisticReducers from "@redux/reducer/collaboration-statistic-reducers";
import corporateContactReducers from "@redux/reducer/corporate-contact-reducers";
import collaborationReducers from "@redux/reducer/collaboration-reducers";
import {
  dashboardPRCardReducers,
  recentCollaborationReducers
} from "@redux/reducer/dashboard-pr-reducers";
import eventReducers from "@redux/reducer/event-reducers";
import partnerTypeReducers from "@redux/reducer/partner-type-reducers";
import partnerReducers from "@redux/reducer/partner-reducers";
import eventStatisticReducers from "@redux/reducer/event-statistic-reducers";
import dashboardEEOReducers from "@redux/reducer/dashboard-eeo-reducers";
import eventDetailReducers from "@redux/reducer/event-detail-reducers";
import majorReducers from "@redux/reducer/major-reducers";
import participantReducers from "@redux/reducer/participant-reducers";
import roleReducers from "@redux/reducer/role-reducers";

const rootReducer = combineReducers({
  corporate: corporateReducers,
  form: formReducers,
  auth: authReducers,
  collaborationStatistic: collaborationStatisticReducers,
  collaborationType: collaborationTypeReducers,
  event: eventReducers,
  corporateContact: corporateContactReducers,
  collaboration: collaborationReducers,
  dashboardPRCard: dashboardPRCardReducers,
  recentCollaboration: recentCollaborationReducers,
  partnerType: partnerTypeReducers,
  eventStatistic: eventStatisticReducers,
  dashboardEEO: dashboardEEOReducers,
  partner: partnerReducers,
  eventDetail: eventDetailReducers,
  major: majorReducers,
  participant: participantReducers,
  role: roleReducers
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
