import { toast } from "react-toastify";

import api from "@api/controllers/event";
import ActionType from "@redux/constants/event-constants";

export const getEvents = (params) => async (dispatch) => {
  try {
    const { data } = await api.getEvents(params);
    const actionType = !!params
      ? ActionType.GET_EVENT
      : ActionType.GET_ALL_EVENTS;
    dispatch({
      type: actionType,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const deleteEvent = (id) => async (dispatch) => {
  try {
    await api.deleteEvent(id);
    dispatch({ type: ActionType.DELETE_EVENT, payload: id });
    toast.success("Successfully Deleted an Event!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const updateEvent = (body, id) => async (dispatch) => {
  try {
    const { data } = await api.updateEvent(id, body);
    dispatch({
      type: ActionType.UPDATE_EVENT,
      payload: data
    });
    toast.success("Successfully Updated a Event!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const createEvent = (form) => async (dispatch) => {
  try {
    const { data } = await api.createEvent(form);
    dispatch({
      type: ActionType.CREATE_EVENT,
      payload: data
    });
    toast.success("Successfully Created a Event!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const getEventById = (id) => async (dispatch) => {
  try {
    const { data } = await api.getEventById(id);
    dispatch({
      type: ActionType.GET_EVENT_BY_ID,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};
