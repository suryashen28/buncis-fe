import { toast } from "react-toastify";

import api from "@api/controllers/partner-type";
import ActionType from "@redux/constants/partner-type-constants";

export const getPartnerTypes = (params) => async (dispatch) => {
  try {
    const { data } = await api.getPartnerTypes(params);
    dispatch({
      type: ActionType.GET_PARTNER_TYPES,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getAllPartnerTypes = () => async (dispatch) => {
  try {
    const { data } = await api.getPartnerTypes();
    dispatch({
      type: ActionType.GET_ALL_PARTNER_TYPES,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const createPartnerType = (form) => async (dispatch) => {
  try {
    const { data } = await api.createPartnerType(form);
    dispatch({
      type: ActionType.CREATE_PARTNER_TYPE,
      payload: data
    });
    toast.success("Success Created a Partner Type!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const getPartnerTypeById = (id) => async (dispatch) => {
  try {
    const { data } = await api.getPartnerTypeById(id);
    dispatch({
      type: ActionType.GET_PARTNER_TYPE_BY_ID,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const updatePartnerType = (body, id) => async (dispatch) => {
  try {
    const { data } = await api.updatePartnerType(id, body);
    dispatch({
      type: ActionType.UPDATE_PARTNER_TYPE,
      payload: data
    });
    toast.success("Success Updated a Partner Type!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const deletePartnerType = (id) => async (dispatch) => {
  try {
    await api.deletePartnerType(id);
    dispatch({ type: ActionType.DELETE_PARTNER_TYPE, payload: id });
    toast.success("Success Deleted a Partner Type!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const resetPartnerTypeForm = () => (dispatch) => {
  dispatch({
    type: ActionType.RESET_PARTNER_TYPE_FORM
  });
};
