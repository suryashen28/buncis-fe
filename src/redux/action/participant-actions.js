import { toast } from "react-toastify";

import api from "@api/controllers/participant";
import ActionType from "@redux/constants/participant-constants";

export const updateParticipant = (body, id, eventId) => async (dispatch) => {
  try {
    const { data } = await api.updateParticipant(eventId, id, body);
    dispatch({
      type: ActionType.UPDATE_PARTICIPANT,
      payload: data
    });
    toast.success("Successfully Updated a Participant!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const createParticipant = (eventId, form) => async (dispatch) => {
  try {
    const { data } = await api.createParticipant(eventId, form);
    dispatch({
      type: ActionType.CREATE_PARTICIPANT,
      payload: data
    });
    toast.success("Successfully Created a Participant!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const getParticipantById = (eventId, id) => async (dispatch) => {
  try {
    const { data } = await api.getParticipantById(eventId, id);
    dispatch({
      type: ActionType.GET_PARTICIPANT_BY_ID,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getParticipantsByPartnerTypeId = (
  eventId,
  partnerTypeId
) => async (dispatch) => {
  try {
    const { data } = await api.getParticipantsByPartnerTypeId(
      eventId,
      partnerTypeId
    );
    dispatch({
      type: ActionType.GET_PARTICIPANT_BY_PARTNER_TYPE_ID,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const deleteParticipant = (eventId, id) => async (dispatch) => {
  try {
    await api.deleteParticipant(eventId, id);
    dispatch({ type: ActionType.DELETE_PARTICIPANT, payload: id });
    toast.success("Successfully Deleted a Participant!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const createParticipants = (eventId, partnerTypeId, form) => async (
  dispatch
) => {
  try {
    const { data } = await api.createParticipants(eventId, partnerTypeId, form);
    dispatch({
      type: ActionType.CREATE_PARTICIPANTS,
      payload: data
    });
    toast.success("Successfully Created Participants!");
  } catch (e) {
    toast.error(e.message);
  }
};
