import { toast } from "react-toastify";

import api from "@api/controllers/event-detail";
import ActionType from "@redux/constants/event-detail-constants";

export const createEventDetailNote = (id, form) => async (dispatch) => {
  try {
    const { data } = await api.createEventDetailNote(id, form);
    dispatch({
      type: ActionType.CREATE_EVENT_DETAIL_NOTE,
      payload: data
    });
    toast.success("Successfully Created an Event Detail Note!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const getEventDetailNote = (id) => async (dispatch) => {
  try {
    const { data } = await api.getEventDetailNote(id);
    dispatch({
      type: ActionType.GET_EVENT_DETAIL_NOTE,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const updateEventDetailNote = (body, id, noteId) => async (dispatch) => {
  try {
    const { data } = await api.updateEventDetailNote(id, noteId, body);
    dispatch({
      type: ActionType.UPDATE_EVENT_DETAIL_NOTE,
      payload: data
    });
    toast.success("Success Updated an Event Detail Note!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const deleteEventDetailNote = (id, noteId) => async (dispatch) => {
  try {
    await api.deleteEventDetailNote(id, noteId);
    dispatch({
      type: ActionType.DELETE_EVENT_DETAIL_NOTE,
      payload: id
    });
    toast.success("Success Deleted an Event Detail Note!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const createEventDetailDocumentation = (id, form) => async (
  dispatch
) => {
  try {
    const { data } = await api.createEventDetailDocumentation(id, form);
    dispatch({
      type: ActionType.CREATE_EVENT_DETAIL_DOCUMENTATION,
      payload: data
    });
    toast.success("Successfully Created an Event Detail Documentation!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const getEventDetailDocumentation = (id) => async (dispatch) => {
  try {
    const { data } = await api.getEventDetailDocumentation(id);
    dispatch({
      type: ActionType.GET_EVENT_DETAIL_DOCUMENTATION,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const updateEventDetailDocumentation = (
  body,
  id,
  documentationId
) => async (dispatch) => {
  try {
    const { data } = await api.updateEventDetailDocumentation(
      id,
      documentationId,
      body
    );
    dispatch({
      type: ActionType.UPDATE_EVENT_DETAIL_DOCUMENTATION,
      payload: data
    });
    toast.success("Success Updated an Event Detail Documentation!");
  } catch (e) {
    toast.error(e.message);
  }
};
