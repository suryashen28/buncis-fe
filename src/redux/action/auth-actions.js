import { toast } from "react-toastify";

import api from "@api/controllers/auth";
import ActionType from "@redux/constants/auth-constants";
import { getAPIData } from "@utils/http-api";

export const login = (form) => async (dispatch) => {
  try {
    const response = await api.login(form);
    const data = getAPIData(response);
    dispatch({
      type: ActionType.LOGIN,
      payload: data
    });
    return data;
  } catch (e) {
    toast.error(e.message);
  }
};
