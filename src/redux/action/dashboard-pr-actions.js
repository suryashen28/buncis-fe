import { toast } from "react-toastify";

import api from "@api/controllers/dashboard-pr";
import ActionType from "@redux/constants/dashboard-pr-constants";

export const getCountCorporateContact = () => async (dispatch) => {
  try {
    const { data } = await api.getCountCorporateContact();
    dispatch({
      type: ActionType.GET_COUNT_CORPORATE_CONTACT,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getCountCollaborationThisYear = () => async (dispatch) => {
  try {
    const { data } = await api.getCountCollaborationThisYear();
    dispatch({
      type: ActionType.GET_COUNT_COLLABORATION_THIS_YEAR,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getRecentCollaboration = () => async (dispatch) => {
  try {
    const { data } = await api.getRecentCollaboration();
    dispatch({
      type: ActionType.GET_RECENT_COLLABORATION,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};
