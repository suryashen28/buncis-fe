import { toast } from "react-toastify";

import api from "@api/controllers/major";
import ActionType from "@redux/constants/major-constants";

export const getMajors = () => async (dispatch) => {
  try {
    const { data } = await api.getMajors();
    dispatch({
      type: ActionType.GET_MAJORS,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getMajorByID = (id) => async (dispatch) => {
  try {
    const { data } = await api.getMajorByID(id);
    dispatch({
      type: ActionType.GET_MAJOR_BY_ID,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const createMajor = (form) => async (dispatch) => {
  try {
    const { data } = await api.createMajor(form);
    dispatch({
      type: ActionType.CREATE_MAJOR,
      payload: data
    });
    toast.success("Successfully Created a Major!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const updateMajor = (body, id) => async (dispatch) => {
  try {
    const { data } = await api.updateMajor(id, body);
    dispatch({
      type: ActionType.UPDATE_MAJOR,
      payload: data
    });
    toast.success("Successfully Updated a Major!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const deleteMajor = (id) => async (dispatch) => {
  try {
    await api.deleteMajor(id);
    dispatch({ type: ActionType.DELETE_MAJOR, payload: id });
    toast.success("Successfully Deleted a Major!");
  } catch (e) {
    toast.error(e.message);
  }
};
