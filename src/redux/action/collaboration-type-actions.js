import { toast } from "react-toastify";

import api from "@api/controllers/collaboration-type";
import ActionType from "@redux/constants/collaboration-type-constants";
import { resetLoading } from "@redux/action/common-actions";

export const getCollaborationTypes = (params) => async (dispatch) => {
  dispatch(resetLoading());
  try {
    const { data } = await api.getCollaborationType(params);
    dispatch({
      type: ActionType.GET_COLLABORATION_TYPES,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getAllCollaborationTypes = () => async (dispatch) => {
  try {
    const { data } = await api.getCollaborationType();
    dispatch({
      type: ActionType.GET_ALL_COLLABORATION_TYPES,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const createCollaborationType = (form) => async (dispatch) => {
  try {
    const { data } = await api.createCollaborationType(form);
    dispatch({
      type: ActionType.CREATE_COLLABORATION_TYPE,
      payload: data
    });
    toast.success("Success Created a Collaboration Type!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const getCollaborationTypeByID = (id) => async (dispatch) => {
  try {
    const { data } = await api.getCollaborationTypeByID(id);
    dispatch({
      type: ActionType.GET_COLLABORATION_TYPE_BY_ID,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const updateCollaborationType = (body, id) => async (dispatch) => {
  try {
    const { data } = await api.updateCollaborationType(id, body);
    dispatch({
      type: ActionType.UPDATE_COLLABORATION_TYPE,
      payload: data
    });
    toast.success("Success Updated a Collaboration Type!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const deleteCollaborationType = (id) => async (dispatch) => {
  try {
    await api.deleteCollaborationType(id);
    dispatch({ type: ActionType.DELETE_COLLABORATION_TYPE, payload: id });
    toast.success("Success Deleted a Collaboration Type!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const resetCollaborationTypeForm = () => (dispatch) => {
  dispatch({
    type: ActionType.RESET_COLLABORATION_TYPE_FORM
  });
};
