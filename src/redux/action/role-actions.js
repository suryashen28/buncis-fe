import { toast } from "react-toastify";

import api from "@api/controllers/role";
import ActionType from "@redux/constants/role-constants";

export const getRoles = () => async (dispatch) => {
  try {
    const { data } = await api.getRoles();
    dispatch({
      type: ActionType.GET_ROLE,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const deleteRole = (id) => async (dispatch) => {
  try {
    await api.deleteRole(id);
    dispatch({ type: ActionType.DELETE_ROLE, payload: id });
    toast.success("Successfully deleted a Role!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const createRole = (form) => async (dispatch) => {
  try {
    const { data } = await api.createRole(form);
    dispatch({
      type: ActionType.CREATE_ROLE,
      payload: data
    });
    toast.success("Successfully Created a Role!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const updateRole = (body, id) => async (dispatch) => {
  try {
    const { data } = await api.updateRole(id, body);
    dispatch({
      type: ActionType.UPDATE_ROLE,
      payload: data
    });
    toast.success("Successfully Updated a Role!");
  } catch (e) {
    toast.error(e.message);
  }
};
