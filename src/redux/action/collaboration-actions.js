import { toast } from "react-toastify";

import api from "@api/controllers/collaboration";
import ActionType from "@redux/constants/collaboration-constants";
import { resetLoading } from "@redux/action/common-actions";

export const getCollaborations = (params) => async (dispatch) => {
  dispatch(resetLoading());
  try {
    const { data } = await api.getCollaborations(params);
    dispatch({
      type: ActionType.GET_COLLABORATIONS,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getCollaborationByID = (id) => async (dispatch) => {
  try {
    const { data } = await api.getCollaborationByID(id);
    dispatch({
      type: ActionType.GET_COLLABORATION_BY_ID,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const createCollaboration = (form) => async (dispatch) => {
  try {
    const { data } = await api.createCollaboration(form);
    dispatch({
      type: ActionType.CREATE_COLLABORATION,
      payload: data
    });
    toast.success("Success Created a Collaboration!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const updateCollaboration = (body, id) => async (dispatch) => {
  try {
    const { data } = await api.updateCollaboration(id, body);
    dispatch({
      type: ActionType.UPDATE_COLLABORATION,
      payload: data
    });
    toast.success("Success Updated a Collaboration!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const deleteCollaboration = (id) => async (dispatch) => {
  try {
    await api.deleteCollaboration(id);
    dispatch({ type: ActionType.DELETE_COLLABORATION, payload: id });
    toast.success("You Success Deleted a Collaboration!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const resetCollaborationForm = () => (dispatch) => {
  dispatch({
    type: ActionType.RESET_COLLABORATION_FORM
  });
};
