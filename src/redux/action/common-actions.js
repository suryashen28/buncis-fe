export const resetLoading = () => (dispatch) => {
  dispatch({
    type: "RESET_LOADING"
  });
};

export const resetDropdownLoading = () => (dispatch) => {
  dispatch({
    type: "RESET_DROPDOWN_LOADING"
  });
};
