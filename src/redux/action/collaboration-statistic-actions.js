import { toast } from "react-toastify";

import api from "@api/controllers/collaboration-statistic";
import ActionType from "@redux/constants/collaboration-statistic-constants";
import { resetLoading } from "@redux/action/common-actions";

const filterData = (data, filter) => {
  const { dataStart, dataEnd } = filter;
  return data.slice(dataStart, dataEnd + 1);
};

export const getCollaborationStatistic = (params, filter) => async (
  dispatch
) => {
  dispatch(resetLoading());
  try {
    const { data } = await api.getCollaborationStatistic(params);
    !!filter && (data.data = filterData(data.data, filter));
    dispatch({
      type: ActionType.GET_COLLABORATION_STATISTIC,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getContactStatistic = (params, filter) => async (dispatch) => {
  dispatch(resetLoading());
  try {
    const { data } = await api.getContactStatistic(params);
    !!filter && (data.data = filterData(data.data, filter));

    dispatch({
      type: ActionType.GET_CONTACT_STATISTIC,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getProfitStatistic = (params, filter) => async (dispatch) => {
  dispatch(resetLoading());
  try {
    const { data } = await api.getProfitStatistic(params);
    !!filter && (data.data = filterData(data.data, filter));
    dispatch({
      type: ActionType.GET_PROFIT_STATISTIC,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};
