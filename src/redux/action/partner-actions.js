import { toast } from "react-toastify";

import api from "@api/controllers/partner";
import ActionType from "@redux/constants/partner-constants";

export const getPartners = (params) => async (dispatch) => {
  try {
    const { data } = await api.getPartners(params);
    dispatch({
      type: ActionType.GET_PARTNERS,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const createPartner = (form) => async (dispatch) => {
  try {
    const { data } = await api.createPartner(form);
    dispatch({
      type: ActionType.CREATE_PARTNER,
      payload: data
    });
    toast.success("Success Created a Partner!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const getPartnerById = (id) => async (dispatch) => {
  try {
    const { data } = await api.getPartnerById(id);
    dispatch({
      type: ActionType.GET_PARTNER_BY_ID,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const updatePartner = (body, id) => async (dispatch) => {
  try {
    const { data } = await api.updatePartner(id, body);
    dispatch({
      type: ActionType.UPDATE_PARTNER,
      payload: data
    });
    toast.success("You Success Updated a Partner!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const deletePartner = (id) => async (dispatch) => {
  try {
    await api.deletePartner(id);
    dispatch({ type: ActionType.DELETE_PARTNER, payload: id });
    toast.success("You Success Delete a Partner!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const resetPartnerForm = () => (dispatch) => {
  dispatch({
    type: ActionType.RESET_PARTNER_FORM
  });
};
