import { toast } from "react-toastify";

import api from "@api/controllers/corporate-contact";
import ActionType from "@redux/constants/corporate-contact-constants";
import {
  resetLoading,
  resetDropdownLoading
} from "@redux/action/common-actions";
import { getAPIData } from "@utils/http-api";

export const getCorporateContacts = (params) => async (dispatch) => {
  dispatch(resetLoading());
  try {
    const { data } = await api.getCorporateContacts(params);
    dispatch({
      type: ActionType.GET_CORPORATE_CONTACTS,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getAllCorporateContacts = () => async (dispatch) => {
  dispatch(resetDropdownLoading());
  try {
    const { data } = await api.getCorporateContacts();
    dispatch({
      type: ActionType.GET_ALL_CORPORATE_CONTACTS,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const createCorporateContact = (form) => async (dispatch) => {
  try {
    const response = await api.createCorporateContact(form);
    const data = getAPIData(response);
    dispatch({
      type: ActionType.CREATE_CORPORATE_CONTACT,
      payload: data
    });
    toast.success("Success Created a Corporate Contact!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const getCorporateContactByID = (id) => async (dispatch) => {
  try {
    const { data } = await api.getCorporateContactByID(id);
    dispatch({
      type: ActionType.GET_CORPORATE_CONTACT_BY_ID,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const updateCorporateContact = (body, id) => async (dispatch) => {
  try {
    const { data } = await api.updateCorporateContact(id, body);
    dispatch({
      type: ActionType.UPDATE_CORPORATE_CONTACT,
      payload: data
    });
    toast.success("Success Updated a Corporate Contact!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const deleteCorporateContact = (id) => async (dispatch) => {
  try {
    await api.deleteCorporateContact(id);
    dispatch({
      type: ActionType.DELETE_CORPORATE_CONTACT,
      payload: id
    });
    toast.success("Success Deleted a Corporate Contact!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const resetCorporateContactForm = () => (dispatch) => {
  dispatch({
    type: ActionType.RESET_CORPORATE_CONTACT_FORM
  });
};
