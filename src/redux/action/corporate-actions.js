import { toast } from "react-toastify";

import api from "@api/controllers/corporate";
import ActionType from "@redux/constants/corporate-constants";
import {
  resetLoading,
  resetDropdownLoading
} from "@redux/action/common-actions";

export const getCorporates = (params) => async (dispatch) => {
  dispatch(resetLoading());
  try {
    const { data } = await api.getCorporates(params);
    dispatch({
      type: ActionType.GET_CORPORATES,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const getAllCorporates = () => async (dispatch) => {
  dispatch(resetDropdownLoading());
  try {
    const { data } = await api.getCorporates();
    dispatch({
      type: ActionType.GET_ALL_CORPORATES,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const createCorporate = (form) => async (dispatch) => {
  try {
    const { data } = await api.createCorporate(form);
    dispatch({
      type: ActionType.CREATE_CORPORATE,
      payload: data
    });
    toast.success("Success Created a Corporate!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const getCorporateByID = (id) => async (dispatch) => {
  try {
    const { data } = await api.getCorporateByID(id);
    dispatch({
      type: ActionType.GET_CORPORATE_BY_ID,
      payload: data
    });
  } catch (e) {
    toast.error(e.message);
  }
};

export const updateCorporate = (body, id) => async (dispatch) => {
  try {
    const { data } = await api.updateCorporate(id, body);
    dispatch({
      type: ActionType.UPDATE_CORPORATE,
      payload: data
    });
    toast.success("Success Updated a Corporate!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const deleteCorporate = (id) => async (dispatch) => {
  try {
    await api.deleteCorporate(id);
    dispatch({
      type: ActionType.DELETE_CORPORATE,
      payload: id
    });
    toast.success("Success Deleted a Corporate!");
  } catch (e) {
    toast.error(e.message);
  }
};

export const resetCorporateForm = () => (dispatch) => {
  dispatch({
    type: ActionType.RESET_CORPORATE_FORM
  });
};
