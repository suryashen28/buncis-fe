import React, { Component, lazy, Suspense, Fragment } from "react";
import { connect } from "react-redux";

import {
  getCountCorporateContact,
  getCountCollaborationThisYear,
  getRecentCollaboration
} from "@redux/action/dashboard-pr-actions";

import TableLoading from "@components/loading/TableLoading";
import ChartLoading from "@components/loading/ChartLoading";
import CardLoading from "@components/loading/CardLoading";

const DashboardChart = lazy(() => import("@pages/dashboard/DashboardChart"));
const DashboardTable = lazy(() => import("@pages/dashboard/DashboardTable"));
const DashboardCard = lazy(() => import("@pages/dashboard/DashboardCard"));

class Dashboard extends Component {
  componentDidMount() {
    const {
      getCountCorporateContact,
      getCountCollaborationThisYear,
      getRecentCollaboration
    } = this.props;
    getCountCorporateContact();
    getCountCollaborationThisYear();
    getRecentCollaboration();
  }

  renderTable = () => {
    const { recentCollaboration, history } = this.props;
    return (
      <div className="table-dashboard">
        <Suspense fallback={<TableLoading />}>
          {recentCollaboration.isLoading ? (
            <TableLoading />
          ) : (
            <DashboardTable
              recentCollaboration={recentCollaboration?.recentCollaboration}
              handleClick={this.handleClick}
              history={history}
            />
          )}
        </Suspense>
      </div>
    );
  };

  renderCard = () => {
    const {
      countCorporateContact,
      countCollaborationThisYear,
      history,
      isLoading
    } = this.props;
    return (
      <Suspense fallback={<CardLoading />}>
        {isLoading ? (
          <CardLoading />
        ) : (
          <DashboardCard
            history={history}
            countContact={countCorporateContact?.countCorporateContacts}
            countCollaborationYear={
              countCollaborationThisYear?.countCollaborationYear
            }
          />
        )}
      </Suspense>
    );
  };

  render() {
    const { history } = this.props;
    return (
      <Fragment>
        <h2 className="content-title">Dashboard</h2>
        <div>{this.renderCard()}</div>
        <div className="content">
          <div className="collaboration-container">
            <Suspense fallback={<ChartLoading />}>
              <DashboardChart history={history} />
            </Suspense>
          </div>
          {this.renderTable()}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = ({
  dashboardPRCard: {
    countCollaborationThisYear,
    countCorporateContact,
    isLoading
  },
  recentCollaboration,
  error
}) => ({
  countCorporateContact,
  countCollaborationThisYear,
  isLoading,
  recentCollaboration,
  error
});

const mapDispatchToProps = (dispatch) => ({
  getCountCorporateContact: () => dispatch(getCountCorporateContact()),
  getCountCollaborationThisYear: () =>
    dispatch(getCountCollaborationThisYear()),
  getRecentCollaboration: () => dispatch(getRecentCollaboration())
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
