import React, { Component, lazy, Suspense } from "react";

import { getThisYear } from "@utils/date";
import routeList from "@src/route";

const Card = lazy(() =>
  import("@components/pages/dashboard/card-dashboard/Card")
);

const renderCards = (history, CARDS) => {
  return (
    <div className="card-deck card-dashboard mt-3 mb-3">
      {CARDS.map((card) => (
        <Card card={card} key={card.id} history={history} />
      ))}
    </div>
  );
};

class DashboardCard extends Component {
  render() {
    const { history, countContact, countCollaborationYear } = this.props;
    const thisYear = getThisYear();
    const { corporateContact, collaboration } = routeList;
    const CARDS = [
      {
        id: 1,
        name: `Contact Saved`,
        total: countContact,
        icon: `users`,
        color: `blue`,
        path: corporateContact.path
      },
      {
        id: 2,
        name: `Collaboration on ${thisYear}`,
        total: countCollaborationYear,
        icon: `id-card-o`,
        color: `green`,
        path: collaboration.path
      }
    ];
    return <div>{renderCards(history, CARDS)}</div>;
  }
}

export default DashboardCard;
