import React, { Component } from "react";

import Table from "@components/common/table/table/Table";
import routeList from "@src/route";

class DashboardTable extends Component {
  columns = [
    { path: "name", label: "Contact Name" },
    { path: "date", label: "Date" }
  ];

  render() {
    const { recentCollaboration, history } = this.props;
    const { collaboration } = routeList;
    return (
      <div>
        <h4 className="table-title">Recent Collaboration</h4>
        <Table columns={this.columns} data={recentCollaboration} />
        <div
          className="content-footer"
          onClick={() => history.push(collaboration.path)}
        >
          View More
        </div>
      </div>
    );
  }
}

export default DashboardTable;
