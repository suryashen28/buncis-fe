import React, { Component } from "react";
import { connect } from "react-redux";
import queryString from "query-string";

import {
  getCollaborationStatistic,
  getContactStatistic,
  getProfitStatistic
} from "@redux/action/collaboration-statistic-actions";
import ChartArea from "@components/common/chart/chart-area/ChartArea";
import { getThisYear } from "@utils/date";
import routeList from "@src/route";
import ChartLoading from "@components/loading/ChartLoading";

const STATISTICS = {
  CONTACT: {
    value: "Contact",
    get: "getContactStatistic"
  },
  COLLABORATION: {
    value: "Collaboration",
    get: "getCollaborationStatistic"
  },
  PROFIT: {
    value: "Profit",
    get: "getProfitStatistic"
  }
};

class DashboardChart extends Component {
  state = {
    isDropdown: false,
    dataStart: 0,
    dataEnd: 0,
    label: []
  };

  componentDidMount() {
    const data = this.getParams();
    Object.keys(data).length !== 0
      ? this.changeStatisticType(data.type, data.filterBy)
      : this.changeStatisticType("Contact", "Month");
  }

  onChange = (type, event) => {
    this.setState({ [type]: parseInt(event.target.value) }, () =>
      this.filterStatisticData(this.state.dataStart, this.state.dataEnd)
    );
  };

  renderLabel = () => {
    const { label } = this.state;
    return label.map((label, index) => (
      <option key={label.name} value={index}>
        {label.name}
      </option>
    ));
  };

  changeStatisticType = async (type, filterBy) => {
    const { isDropdown } = this.state;
    const thisYear = getThisYear();
    const params = {
      filterBy,
      period: thisYear
    };
    await this.props[STATISTICS[type.toUpperCase()].get](params);
    !!isDropdown || this.toggleDropdown();
    const { history, collaborationStatistic } = this.props;
    history.push({ search: `type=${type}&filterBy=${filterBy}` });
    this.setState({
      label: collaborationStatistic,
      dataStart: 0,
      dataEnd: 0
    });
  };

  getParams = () => {
    const { history } = this.props;
    return queryString.parse(history.location.search);
  };

  filterStatisticData = (dataStart, dataEnd) => {
    const filter = { dataStart, dataEnd };
    const { type, filterBy } = this.getParams();
    const thisYear = getThisYear();
    dataStart <= dataEnd &&
      this.props[STATISTICS[type.toUpperCase()].get](
        { filterBy, period: thisYear },
        filter
      );
  };

  renderDropdown = () => {
    const data = this.getParams();
    return this.state.isDropdown === false ? (
      <span className="dropdown">
        <label className="text">{data.filterBy}</label>
        <div className="dropdown-container">
          <li
            onClick={() => this.changeStatisticType(data.type, "Month")}
            className="btn-bar option"
          >
            Month
          </li>
          <li
            onClick={() => this.changeStatisticType(data.type, "Year")}
            className="btn-bar option"
          >
            Year
          </li>
        </div>
      </span>
    ) : (
      <label className="text">{data.filterBy}</label>
    );
  };

  toggleDropdown = () => {
    const { isDropdown } = this.state;
    this.setState({ isDropdown: !isDropdown });
  };

  renderColor = (label) => {
    const data = this.getParams();
    return data.type === label ? "theme" : "black";
  };

  renderType = () => {
    const keys = Object.keys(STATISTICS);
    return keys.map((k) => (
      <span
        key={k}
        onClick={() => this.changeStatisticType(STATISTICS[k].value, "Month")}
        className={`collaboration btn-sm m-2 ${this.renderColor(
          STATISTICS[k].value
        )}`}
      >
        {STATISTICS[k].value}
      </span>
    ));
  };

  renderTitle = () => {
    const title = this.getParams();
    return title.type;
  };

  renderChart = () => {
    const { history, collaborationStatistic, isLoading } = this.props;
    const { collaboration } = routeList;
    return (
      <div>
        <h6>Number of {this.renderTitle()}</h6>
        <span className="select-options">
          {this.renderDropdown()}
          <span
            className="fa fa-lg fa-angle-down"
            onClick={() => this.toggleDropdown()}
          />
        </span>
        <div className="select-inline">
          <select
            className="select-filter"
            onChange={(event) => this.onChange("dataStart", event)}
          >
            {this.renderLabel()}
          </select>
          <select
            className="select-filter "
            onChange={(event) => this.onChange("dataEnd", event)}
          >
            {this.renderLabel()}
          </select>
        </div>
        <div className="btn-right">{this.renderType()}</div>
        {isLoading ? (
          <ChartLoading />
        ) : (
          <ChartArea data={collaborationStatistic} />
        )}
        <div
          className="content-footer theme"
          onClick={() => history.push(collaboration.path)}
        >
          View more
        </div>
      </div>
    );
  };

  render() {
    return <div>{this.renderChart()}</div>;
  }
}

const mapStateToProps = ({
  collaborationStatistic: { collaborationStatistic, error, isLoading }
}) => ({
  collaborationStatistic,
  isLoading,
  error
});

const mapDispatchToProps = (dispatch) => ({
  getCollaborationStatistic: (params, filter) =>
    dispatch(getCollaborationStatistic(params, filter)),
  getContactStatistic: (params, filter) =>
    dispatch(getContactStatistic(params, filter)),
  getProfitStatistic: (params, filter) =>
    dispatch(getProfitStatistic(params, filter))
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardChart);
