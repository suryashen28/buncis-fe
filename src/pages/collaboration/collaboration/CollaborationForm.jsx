import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Form, Field, reduxForm, formValueSelector } from "redux-form";
import { connect } from "react-redux";

import { required, number } from "@utils/form-validation";
import {
  createCollaboration,
  updateCollaboration,
  resetCollaborationForm,
  getCollaborationByID
} from "@redux/action/collaboration-actions";
import { parseToString } from "@utils/date";
import { getAllCollaborationTypes } from "@redux/action/collaboration-type-actions";
import { getEvents } from "@redux/action/event-actions";
import { getAllCorporateContacts } from "@redux/action/corporate-contact-actions";
import { getAllCorporates } from "@redux/action/corporate-actions";
import DateSelector from "@components/common/date-picker/DateSelector";
import Select from "@components/common/select/Select";
import TextArea from "@components/common/textarea/TextArea";
import Input from "@components/common/input/Input";
import routeList from "@src/route";

class CollaborationForm extends Component {
  state = { isChecked: false };

  toggleCheckbox = () => {
    const { isChecked } = this.state;
    this.setState({ isChecked: !isChecked });
  };

  componentDidMount() {
    const {
      getAllCollaborationTypes,
      getEvents,
      getAllCorporateContacts,
      getAllCorporates,
      getCollaborationByID,
      resetForm,
      updatedValues
    } = this.props;
    resetForm("CollaborationForm");
    !!this.getCollaborationId() &&
      getCollaborationByID(this.getCollaborationId());
    getAllCollaborationTypes();
    getEvents();
    getAllCorporateContacts();
    getAllCorporates();
    !!!updatedValues.event && this.toggleCheckbox();
  }

  renderDropdown = (contents, name) => {
    return contents.map((content) => (
      <option key={content.id}>{content[name]}</option>
    ));
  };

  getCollaborationId = () => this.props.match.params.id;

  getFormTitle = () => (!!this.getCollaborationId() ? "Edit" : "Add");

  getSlicedData = (updatedValues) => {
    const keys = [
      "collaborationTypeId",
      "corporateId",
      "date",
      "profit",
      "corporateContactId",
      "description"
    ];
    return keys.reduce((key, index) => {
      !!updatedValues[index] && (key[index] = updatedValues[index]);
      return key;
    }, {});
  };

  getValidValueInForm = () => {
    const { updatedValues } = this.props;
    const { date } = updatedValues;
    const dateParsedToString = parseToString(date);
    const validValues = { ...updatedValues, date: dateParsedToString };
    return this.state.isChecked ? this.getSlicedData(validValues) : validValues;
  };

  handleSubmit = () => {
    const {
      createCollaboration,
      updateCollaboration,
      resetForm,
      history
    } = this.props;

    const validValueInForm = this.getValidValueInForm();
    const { collaboration } = routeList;
    !!this.getCollaborationId()
      ? updateCollaboration(validValueInForm, this.getCollaborationId())
      : createCollaboration(validValueInForm);
    resetForm("CollaborationForm");
    history.push(collaboration.path);
  };

  render() {
    const {
      history,
      valid,
      collaborationTypeOptions,
      initialValues,
      corporateOptions,
      corporateContactOptions
    } = this.props;
    const { isChecked } = this.state;
    const FIELDS = [
      {
        classInput: "form-control-text",
        name: "collaborationTypeId",
        type: "select",
        component: Select,
        label: "Collaboration Type",
        title: "type",
        options: collaborationTypeOptions,
        placeholder: "Select Collaboration Type",
        initialValues: initialValues.collaborationType,
        validate: [required]
      },
      {
        classInput: "form-control-text",
        name: "corporateId",
        type: "select",
        component: Select,
        label: "Corporate",
        title: "name",
        options: corporateOptions,
        placeholder: "Select Corporate",
        initialValues: initialValues.corporate,
        validate: [required]
      },
      {
        classInput: "form-control-text",
        name: "date",
        type: "date",
        component: DateSelector,
        label: "Date",
        validate: [required]
      },
      {
        classInput: "form-control-text",
        name: "profit",
        type: "text",
        component: Input,
        label: "Profit",
        validate: [required, number]
      },
      {
        classInput: "form-control-text",
        name: "corporateContactId",
        type: "select",
        component: Select,
        label: "Contact Person",
        title: "name",
        placeholder: "Select Contact Person",
        options: corporateContactOptions,
        initialValues: initialValues.corporateContact,
        validate: [required]
      },
      {
        classInput: "form-control",
        name: "description",
        type: "area",
        component: TextArea,
        label: "Collaboration Description",
        placeholder: "Enter Collaboration Description..",
        validate: [required]
      }
    ];

    const renderFormFields = () =>
      FIELDS.map((f) => <Field key={f.name} {...f} />);

    const renderEventRow = () => {
      const { isChecked } = this.state;
      const { initialValues, eventOptions } = this.props;
      return (
        <div>
          <Field
            classInput="form-control-text"
            name="eventId"
            component={Select}
            options={eventOptions}
            title="name"
            placeholder="Select Event"
            initialValues={initialValues.eventID}
            isDisabled={isChecked}
          />
        </div>
      );
    };

    const renderForm = () => {
      return (
        <div className="form-container">
          <Form onSubmit={this.handleSubmit}>
            {renderFormFields()}
            <div className="form-event">
              <div className="checkbox">
                <input
                  type="checkbox"
                  onChange={this.toggleCheckbox}
                  defaultChecked={isChecked}
                />
                Collaboration for Event
              </div>
              {renderEventRow()}
            </div>
          </Form>
        </div>
      );
    };

    const { collaboration } = routeList;
    const renderButton = () => {
      return (
        <div className="action-button">
          <button
            className="btn btn-danger btn-sm m-2 button-header"
            onClick={() => history.push(collaboration.path)}
          >
            Cancel
          </button>
          <button
            className="btn btn-success btn-sm m-2 button-header"
            onClick={this.handleSubmit}
            type="submit"
            disabled={!valid}
          >
            Save
          </button>
        </div>
      );
    };
    return (
      <div>
        <div className="collaboration-form">
          <div className="collaboration-form-container">
            <h1 className="content-title">
              {this.getFormTitle()} Collaboration
            </h1>
          </div>
        </div>
        {renderForm()}
        <div>{renderButton()}</div>
      </div>
    );
  }
}

const selector = formValueSelector("CollaborationForm");

const mapStateToProps = (state) => {
  const {
    collaborationType: { collaborationTypeOptions },
    event: { eventOptions },
    corporateContact: { corporateContactOptions },
    corporate: { corporateOptions },
    loading,
    error
  } = state;
  return {
    collaborationTypeOptions,
    corporateContactOptions,
    corporateOptions,
    eventOptions,
    loading,
    error,
    initialValues: state.collaboration.form,
    updatedValues: selector(
      state,
      "collaborationTypeId",
      "corporateId",
      "date",
      "corporateContactId",
      "description",
      "eventId",
      "profit"
    )
  };
};

const mapDispatchToProps = (dispatch) => ({
  getAllCollaborationTypes: () => dispatch(getAllCollaborationTypes()),
  getAllCorporateContacts: () => dispatch(getAllCorporateContacts()),
  getEvents: () => dispatch(getEvents()),
  getAllCorporates: () => dispatch(getAllCorporates()),
  createCollaboration: (data) => dispatch(createCollaboration(data)),
  updateCollaboration: (data, id) => dispatch(updateCollaboration(data, id)),
  getCollaborationByID: (id) => dispatch(getCollaborationByID(id)),
  resetForm: () => dispatch(resetCollaborationForm())
});

CollaborationForm = reduxForm({
  form: "CollaborationForm",
  multipartForm: true,
  enableReinitialize: true
})(withRouter(CollaborationForm));

export default connect(mapStateToProps, mapDispatchToProps)(CollaborationForm);
