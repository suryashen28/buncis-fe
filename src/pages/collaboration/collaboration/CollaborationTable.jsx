import React, { Component } from "react";

import Table from "@components/common/table/table/Table";
import CollaborationModal from "@components/pages/collaboration/CollaborationModal";
import routeList from "@src/route";

class CorporateContactTable extends Component {
  COLUMNS = () => {
    const { history, handleDelete } = this.props;
    const { collaboration } = routeList;
    const onExit = () => {
      history.push(collaboration);
    };

    const columns = [
      { path: "collaborationType", subPath: "name", label: "Type" },
      { path: "corporate", label: "Corporate Name", subPath: "name" },
      { path: "description", label: "Description" },
      { path: "date", label: "Date" },
      {
        path: "corporateContact",
        subPath: "name",
        label: "Contact Person"
      },
      {
        path: "profit",
        label: "Profit"
      },
      {
        key: "collaborationActions",
        path: "Actions",
        content: (collaboration) => (
          <div className="collaboration-actions">
            <i
              className="fa fa-pencil fa-xl orange option"
              onClick={() =>
                history.push(
                  `${routeList.collaboration.path}/${collaboration.id}/edit`
                )
              }
            />
            <CollaborationModal
              onExit={onExit}
              collaboration={collaboration}
              history={history}
              handleDelete={handleDelete}
            />
          </div>
        ),
        label: "Actions"
      }
    ];
    return columns;
  };

  render() {
    const { collaborations } = this.props;
    return (
      <div>
        <Table columns={this.COLUMNS(collaborations)} data={collaborations} />
      </div>
    );
  }
}

export default CorporateContactTable;
