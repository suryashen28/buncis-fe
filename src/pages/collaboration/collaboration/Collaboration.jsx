import React, { Component, lazy, Suspense } from "react";
import { connect } from "react-redux";
import queryString from "query-string";

import { getCollaborations } from "@redux/action/collaboration-actions";
import { deleteCollaboration } from "@redux/action/collaboration-actions";
import { getAllCorporateContacts } from "@redux/action/corporate-contact-actions";
import { getAllCorporates } from "@redux/action/corporate-actions";
import { getListOfPassedYears } from "@utils/date";
import Dropdown from "@components/common/dropdown/Dropdown";
import routeList from "@src/route";
import Pagination from "@components/common/pagination/Pagination";
import SelectForm from "@components/common/select/SelectForm";
import PaginationLoading from "@components/loading/PaginationLoading";
import TableLoading from "@components/loading/TableLoading";
import DropdownLoading from "@components/loading/DropdownLoading";

const CollaborationTable = lazy(() =>
  import("@pages/collaboration/collaboration/CollaborationTable")
);

const FILTER_TYPE = [
  {
    filterName: "Corporate",
    get: "getAllCorporates",
    name: "corporateOptions",
    filterBy: "corporate"
  },
  {
    filterName: "Contact Person",
    get: "getAllCorporateContacts",
    name: "corporateContactOptions",
    filterBy: "corporateContact"
  }
];

class Collaboration extends Component {
  state = {
    isDropdown: false,
    filterBy: "",
    currentSelectedDropdown: "",
    selectedId: ""
  };

  toggleDropdown = () => {
    this.setState({ isDropdown: !this.state.isDropdown });
  };

  setName = (name) => {
    const { isDropdown, currentSelectedDropdown } = this.state;
    name = !isDropdown || currentSelectedDropdown !== name ? name : "";
    this.setState({ currentSelectedDropdown: name });
  };

  getParams = () => {
    const { history } = this.props;
    return queryString.parse(history.location.search);
  };

  componentDidMount() {
    const { history, page } = this.props;
    const id = history.location.state?.id;
    const currentPageParams = this.getParams();
    !!id
      ? this.setState(
          {
            selectedId: id,
            filterBy: currentPageParams.filterBy
          },
          () => this.getCollaborations({ page: page.page })
        )
      : this.getCollaborations({ page: page.page });
    !currentPageParams.page && history.push({ search: `page=1` });
  }

  getCollaborationsByPage = (page) => {
    this.getCollaborations({ page: page });
  };

  getCollaborations = (params) => {
    const { getCollaborations } = this.props;
    getCollaborations(params);
  };

  onFilterSubmit = (result) => {
    const { history } = this.props;
    const params = {
      page: 1,
      ...result
    };
    const urlParams = queryString.stringify(params);
    history.push({ search: urlParams });
    this.getCollaborations(params);
  };

  handleFilter = (type) => {
    const { get, filterBy } = type;
    const { currentSelectedDropdown } = this.state;
    (currentSelectedDropdown === filterBy || !currentSelectedDropdown) &&
      this.toggleDropdown();
    this.setName(filterBy);
    this.props[get]();
  };

  handleDropdownDataChanges = (data) => {
    const { getCollaborations, history, page } = this.props;
    const { selectedId, currentSelectedDropdown } = this.state;
    const updatedPage = selectedId === data.id ? page.page : 1;
    const params = {
      filterBy: currentSelectedDropdown,
      page: updatedPage,
      id: data.id
    };
    getCollaborations(params);
    this.setState({ selectedId: data.id, filterBy: currentSelectedDropdown });
    history.push({
      search: `filterBy=${currentSelectedDropdown}&query=${data.name}&page=1`
    });
  };

  renderFilterContent = () => {
    const { currentSelectedDropdown } = this.state;
    const type = FILTER_TYPE.find(
      (type) => type.filterBy === currentSelectedDropdown
    );
    return (
      !!this.state.isDropdown && (
        <div className="collaboration-list">
          {this.props[currentSelectedDropdown]?.isDropdownLoading ? (
            <DropdownLoading />
          ) : (
            <Dropdown
              content={this.props[type.filterBy][type.name]}
              handleSubmit={this.handleDropdownDataChanges}
              name="collaboration-filter-content"
            />
          )}
        </div>
      )
    );
  };

  renderDropdownArrow = (name) => {
    const type =
      this.state.currentSelectedDropdown === name && this.state.isDropdown
        ? "up"
        : "down";
    return <i className={`fa fa-angle-${type} fa-lg`} />;
  };

  renderFilterType = () => {
    return FILTER_TYPE.map((type) => (
      <span
        className="collaboration-filter-by"
        key={type.filterName}
        onClick={() => this.handleFilter(type)}
      >
        {type.filterName}
        {this.renderDropdownArrow(type.filterBy)}
      </span>
    ));
  };

  resetFilter = () => {
    const { getCollaborations, history } = this.props;
    const params = {
      page: 1
    };
    getCollaborations(params);
    this.setState({ selectedId: "" });
    history.push({ search: `page=1` });
  };

  renderFilter = () => {
    return (
      <div className="collaboration-filter">
        <div>{this.renderFilterType()}</div>
        <div className="collaboration-filter-content-container">
          {this.renderFilterContent()}
        </div>
        <button className="btn btn-reset" onClick={() => this.resetFilter()}>
          Reset Filter
        </button>
      </div>
    );
  };

  render() {
    const {
      collaboration,
      history,
      deleteCollaboration,
      page,
      isLoading
    } = this.props;
    const { collaborationForm } = routeList;
    const numberOfPassedYears = 10;
    const yearOptions = getListOfPassedYears(numberOfPassedYears);
    return (
      <div>
        <div className="layout-style">
          <h2 className="content-title">Collaboration</h2>
          <SelectForm
            options={yearOptions}
            placeholder="Select Year"
            label="Year"
            title="id"
            name="year"
            onChange={this.onFilterSubmit}
          />
          <button
            className="btn btn-info btn-sm m-2"
            onClick={() => history.push(collaborationForm.path.add)}
          >
            Add New Collaboration
          </button>
        </div>
        <div className="row content">
          {this.renderFilter()}
          <div className="collaboration-details">
            <div className="collaboration-table">
              <Suspense fallback={<TableLoading />}>
                {isLoading ? (
                  <TableLoading />
                ) : (
                  <CollaborationTable
                    collaborations={collaboration}
                    handleDelete={deleteCollaboration}
                    history={history}
                  />
                )}
              </Suspense>
            </div>
          </div>
        </div>
        <Suspense fallback={<PaginationLoading />}>
          <Pagination
            numberOfPages={page.totalPage}
            currentPage={page.page}
            history={history}
            isLoading={isLoading}
            onChange={this.getCollaborationsByPage}
          />
        </Suspense>
      </div>
    );
  }
}

const mapStateToProps = ({
  collaboration: { collaboration, page, isLoading },
  corporate,
  corporateContact,
  isDropdownLoading,
  error
}) => ({
  collaboration,
  corporate,
  corporateContact,
  isDropdownLoading,
  isLoading,
  error,
  page
});

const mapDispatchToProps = (dispatch) => ({
  getCollaborations: (params) => dispatch(getCollaborations(params)),
  deleteCollaboration: (id) => dispatch(deleteCollaboration(id)),
  getAllCorporates: () => dispatch(getAllCorporates()),
  getAllCorporateContacts: () => dispatch(getAllCorporateContacts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Collaboration);
