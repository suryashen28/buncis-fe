import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Form, Field, reduxForm, formValueSelector } from "redux-form";

import {
  createCollaborationType,
  updateCollaborationType,
  getCollaborationTypeByID,
  resetCollaborationTypeForm
} from "@redux/action/collaboration-type-actions";
import Input from "@components/common/input/Input";
import TextArea from "@components/common/textarea/TextArea";
import { required } from "@utils/form-validation.js";
import routeList from "@src/route";

const FIELDS = [
  {
    classInput: "form-control-text",
    name: "type",
    label: "Collaboration Type",
    component: Input,
    type: "text",
    placeholder: "Enter Collaboration Type",
    id: "input-collaboration-type",
    validate: [required]
  },
  {
    classInput: "form-control",
    name: "description",
    label: "Collaboration Description",
    component: TextArea,
    type: "area",
    placeholder: "Enter Collaboration Description",
    id: "input-collaboration-description",
    validate: [required]
  }
];

class CollaborationTypeForm extends Component {
  renderFormFields = () =>
    FIELDS.map((field) => <Field key={field.name} {...field} />);

  componentDidMount() {
    const { getCollaborationTypeByID, resetForm } = this.props;
    resetForm("CollaborationTypeForm");
    !!this.getCollaborationTypeId() &&
      getCollaborationTypeByID(this.getCollaborationTypeId());
  }

  handleSubmit = () => {
    const {
      updatedValues,
      createCollaborationType,
      updateCollaborationType,
      resetForm,
      history
    } = this.props;

    const { collaborationType } = routeList;
    !!this.getCollaborationTypeId()
      ? updateCollaborationType(updatedValues, this.getCollaborationTypeId())
      : createCollaborationType(updatedValues);
    resetForm("CollaborationTypeForm");
    history.push(collaborationType.path);
  };

  getCollaborationTypeId = () => this.props.match.params.id;
  getFormTitle = () => (!!this.getCollaborationTypeId() ? "Edit" : "Add");

  renderButton = () => {
    const { history, valid } = this.props;
    const { collaborationType } = routeList;
    return (
      <div className="action-button">
        <button
          className="btn button-style btn-danger btn-sm m-2"
          onClick={() => history.push(collaborationType.path)}
        >
          Cancel
        </button>
        <button
          className="btn button-style btn-success btn-sm m-2"
          onClick={() => this.handleSubmit()}
          disabled={!valid}
          type="submit"
        >
          Save
        </button>
      </div>
    );
  };

  render() {
    return (
      <div>
        <div className="layout-style">
          <div>
            <h1 className="content-title">
              {this.getFormTitle()} Collaboration Type
            </h1>
          </div>
        </div>
        <div className="form-container">
          <Form onSubmit={this.handleSubmit}>{this.renderFormFields()}</Form>
        </div>
        {this.renderButton()}
      </div>
    );
  }
}

const selector = formValueSelector("CollaborationTypeForm");

const mapStateToProps = (state) => {
  return {
    initialValues: state.collaborationType.form,
    updatedValues: selector(state, "type", "description")
  };
};

const mapDispatchToProps = (dispatch) => ({
  createCollaborationType: (data) => dispatch(createCollaborationType(data)),
  updateCollaborationType: (data, id) =>
    dispatch(updateCollaborationType(data, id)),
  getCollaborationTypeByID: (id) => dispatch(getCollaborationTypeByID(id)),
  resetForm: () => dispatch(resetCollaborationTypeForm())
});

CollaborationTypeForm = reduxForm({
  form: "CollaborationTypeForm",
  enableReinitialize: true
})(withRouter(CollaborationTypeForm));

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CollaborationTypeForm);
