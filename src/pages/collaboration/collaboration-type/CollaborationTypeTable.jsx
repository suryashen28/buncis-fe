import React, { Component } from "react";

import Table from "@components/common/table/table/Table";
import CollaborationTypeModal from "@components/pages/collaboration-type/CollaborationTypeModal";
import routeList from "@src/route";

class CollaborationTypeTable extends Component {
  COLUMNS = () => {
    const { history, handleDelete } = this.props;

    const columns = [
      { path: "type", label: "Collaboration Type" },
      { path: "description", label: "Collaboration Description" },
      {
        path: "Actions",
        label: "Actions",
        key: "viewDetails",
        content: (collaborationType) => (
          <div className="collaboration-actions">
            <i
              className="fa fa-pencil fa-xl orange option"
              onClick={() =>
                history.push(
                  `${routeList.collaborationType.path}/${collaborationType.id}/edit`
                )
              }
            />
            <CollaborationTypeModal
              collaborationType={collaborationType}
              history={history}
              handleDelete={handleDelete}
            />
          </div>
        )
      }
    ];
    return columns;
  };
  renderCollaborationTypeTable = () => {
    const { collaborationType } = this.props;
    return (
      <Table
        columns={this.COLUMNS(collaborationType)}
        data={collaborationType}
      />
    );
  };

  render() {
    return this.renderCollaborationTypeTable();
  }
}

export default CollaborationTypeTable;
