import React, { Component, Suspense, lazy } from "react";
import { connect } from "react-redux";
import queryString from "query-string";

import {
  getCollaborationTypes,
  deleteCollaborationType
} from "@redux/action/collaboration-type-actions";
import routeList from "@src/route";
import Pagination from "@components/common/pagination/Pagination";
import PaginationLoading from "@components/loading/PaginationLoading";
import TableLoading from "@components/loading/TableLoading";

const CollaborationTypeTable = lazy(() =>
  import("@pages/collaboration/collaboration-type/CollaborationTypeTable")
);

class CollaborationType extends Component {
  getCollaborationTypes = (page) => {
    const { getCollaborationTypes } = this.props;
    getCollaborationTypes({ page: page });
  };

  componentDidMount() {
    const MINIMUM_NUMBER_OF_PAGES = 1;
    const { history, page } = this.props;
    !queryString.parse(history.location.search).page &&
      history.push({ search: `page=${MINIMUM_NUMBER_OF_PAGES}` });
    this.getCollaborationTypes(page.page);
  }

  render() {
    const {
      collaborationType,
      history,
      deleteCollaborationType,
      page,
      isLoading
    } = this.props;
    return (
      <div>
        <div className="position-header-content">
          <h2 className="content-title">Collaboration Type</h2>
          <button
            className="btn btn-info btn-sm m-2"
            onClick={() =>
              history.push(routeList.collaborationTypeForm.path.add)
            }
          >
            Add New Collaboration Type
          </button>
        </div>
        <div className="collaboration-type content">
          <div className="collaboration-type-table">
            <Suspense fallback={<TableLoading />}>
              {isLoading ? (
                <TableLoading />
              ) : (
                <CollaborationTypeTable
                  collaborationType={collaborationType}
                  history={history}
                  handleDelete={deleteCollaborationType}
                />
              )}
            </Suspense>
          </div>
        </div>
        <Suspense fallback={<PaginationLoading />}>
          <Pagination
            numberOfPages={page.totalPage}
            currentPage={page.page}
            history={history}
            isLoading={isLoading}
            onChange={this.getCollaborationTypes}
          />
        </Suspense>
      </div>
    );
  }
}

const mapStateToProps = ({
  collaborationType: { collaborationType, error, page, isLoading }
}) => ({
  collaborationType,
  isLoading,
  error,
  page
});

const mapDispatchToProps = (dispatch) => ({
  getCollaborationTypes: (params) => dispatch(getCollaborationTypes(params)),
  deleteCollaborationType: (id) => dispatch(deleteCollaborationType(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(CollaborationType);
