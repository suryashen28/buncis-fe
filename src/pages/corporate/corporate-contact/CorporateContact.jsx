import React, { Component, lazy, Suspense } from "react";
import { connect } from "react-redux";
import queryString from "query-string";

import { getAllCorporates } from "@redux/action/corporate-actions";
import {
  getCorporateContacts,
  deleteCorporateContact
} from "@redux/action/corporate-contact-actions";

import Dropdown from "@components/common/dropdown/Dropdown";
import routeList from "@src/route";
import Pagination from "@components/common/pagination/Pagination";
import Search from "@components/common/search/Search";
import PaginationLoading from "@components/loading/PaginationLoading";
import TableLoading from "@components/loading/TableLoading";
import DropdownLoading from "@components/loading/DropdownLoading";

const FILTER_TYPE = [
  { filterName: "Corporate", get: "getAllCorporates", name: "corporateOptions" }
];
const CorporateContactTable = lazy(() =>
  import("@pages/corporate/corporate-contact/CorporateContactTable")
);
class CorporateContact extends Component {
  state = {
    isDropdown: false,
    selectedId: "",
    selectedName: "",
    search: ""
  };

  toggleDropdown = () => {
    this.setState({ isDropdown: !this.state.isDropdown });
  };

  setName = (name) => {
    const { isDropdown, selectedName } = this.state;
    name = !isDropdown || selectedName !== name ? name : "";
    this.setState({ selectedName: name });
  };

  getParams = () => {
    const { history } = this.props;
    return queryString.parse(history.location.search);
  };

  componentDidMount() {
    const { history, page } = this.props;
    const id = history.location.state?.id;
    const currentPageParams = this.getParams();
    !!id
      ? this.setState({ selectedId: id, search: this.state.search }, () =>
          this.getCorporateContacts(page.page)
        )
      : this.getCorporateContacts(page.page);
    !currentPageParams.page && history.push({ search: `page=1` });
  }

  setName = (name) => {
    const { isDropdown, selectedName } = this.state;
    name = !isDropdown || selectedName !== name ? name : "";
    this.setState({ selectedName: name });
  };

  handleFilter = (type) => {
    const { get, name } = type;
    const { selectedName } = this.state;
    (selectedName === name || !selectedName) && this.toggleDropdown();
    this.setName(name);
    this.props[get]();
  };

  handleDropdownDataChanges = (data) => {
    const { id, name } = data;
    const { getCorporateContacts, history, page } = this.props;
    const { selectedId } = this.state;
    const updatedPage = selectedId === id ? page.page : 1;
    const params = { corporateId: id, page: updatedPage };
    getCorporateContacts(params);
    this.setState({ selectedId: id });
    history.push({ search: `query=${name}&page=1` });
  };

  renderFilterContent = () => {
    const { selectedName, isDropdown } = this.state;
    const { isDropdownLoading } = this.props;
    return (
      !!isDropdown && (
        <div className="collaboration-list">
          {isDropdownLoading ? (
            <DropdownLoading />
          ) : (
            <Dropdown
              content={this.props[selectedName]}
              handleSubmit={this.handleDropdownDataChanges}
              name="collaboration-filter-content"
            />
          )}
        </div>
      )
    );
  };

  renderDropdownArrow = (name) => {
    const { selectedName, isDropdown } = this.state;
    const type = selectedName === name && isDropdown ? "up" : "down";
    return <i className={`fa fa-angle-${type} fa-lg`} />;
  };

  renderFilterType = () => {
    return FILTER_TYPE.map((type) => (
      <span
        className="collaboration-filter-by"
        key={type.filterName}
        onClick={() => this.handleFilter(type)}
      >
        {type.filterName}
        {this.renderDropdownArrow(type.name)}
      </span>
    ));
  };

  resetFilter = () => {
    const { getCorporateContacts, history } = this.props;
    const params = {
      page: 1
    };
    getCorporateContacts(params);
    this.setState({ selectedId: "" });
    history.push({ search: `page=1` });
  };

  renderFilter = () => {
    return (
      <div className="collaboration-filter">
        <div>{this.renderFilterType()}</div>
        <div className="collaboration-filter-content-container">
          {this.renderFilterContent()}
        </div>
        <button className="btn btn-reset" onClick={() => this.resetFilter()}>
          Reset Filter
        </button>
      </div>
    );
  };

  getCorporateContacts = (page) => {
    const { getCorporateContacts } = this.props;
    const requestParams =
      this.state.selectedId.length === 0
        ? {
            page: page,
            search: this.state.search
          }
        : {
            page: page,
            corporateId: this.state.selectedId
          };
    getCorporateContacts(requestParams);
  };

  onSearchSubmit = (result) => {
    this.setState({ search: result.search });
    const params = {
      page: 1,
      ...result
    };
    this.props.getCorporateContacts(params);
  };

  render() {
    const {
      corporateContact,
      history,
      deleteCorporateContact,
      page,
      isLoading
    } = this.props;
    const currentPageParams = this.getParams();
    return (
      <div>
        <div className="layout-style">
          <h2 className="content-title">Corporate Contact</h2>
          <Search onSearchSubmit={this.onSearchSubmit} />
          <button
            className="btn btn-info btn-sm m-2"
            onClick={() =>
              history.push(routeList.corporateContactForm.path.add)
            }
          >
            Add New Corporate Contact
          </button>
        </div>
        <div className="row content">
          {this.renderFilter()}
          <div className="collaboration-details">
            <div className="collaboration-table">
              <Suspense fallback={<TableLoading />}>
                {isLoading ? (
                  <TableLoading />
                ) : (
                  <CorporateContactTable
                    corporateContact={corporateContact}
                    history={history}
                    handleDelete={deleteCorporateContact}
                  />
                )}
              </Suspense>
            </div>
          </div>
        </div>
        <Suspense fallback={<PaginationLoading />}>
          <Pagination
            numberOfPages={page.totalPage}
            currentPage={page.page}
            history={history}
            isLoading={isLoading}
            currentPageParams={currentPageParams}
            onChange={this.getCorporateContacts}
          />
        </Suspense>
      </div>
    );
  }
}

const mapStateToProps = ({
  corporateContact: { corporateContact, page, isLoading },
  corporate: { corporate, corporateOptions, isDropdownLoading },
  error
}) => ({
  corporate,
  isDropdownLoading,
  corporateOptions,
  corporateContact,
  isLoading,
  error,
  page
});

const mapDispatchToProps = (dispatch) => ({
  getAllCorporates: () => dispatch(getAllCorporates()),
  getCorporateContacts: (params) => dispatch(getCorporateContacts(params)),
  deleteCorporateContact: (id) => dispatch(deleteCorporateContact(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(CorporateContact);
