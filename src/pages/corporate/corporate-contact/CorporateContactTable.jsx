import React, { Component } from "react";

import Table from "@components/common/table/table/Table";
import CorporateContactModal from "@components/pages/corporate-contact/CorporateContactModal";
import routeList from "@src/route";

class CorporateContactTable extends Component {
  COLUMNS = () => {
    const { history, handleDelete } = this.props;
    const { corporateContact } = routeList;
    const onExit = () => {
      history.push(corporateContact.path);
    };

    const columns = [
      { path: "name", label: "Name" },
      { path: "corporate", subPath: "name", label: "Corporate Name" },
      { path: "position", label: "Position" },
      { path: "email", label: "Email" },
      { path: "phoneNumber", label: "Phone" },
      {
        key: "changeData",
        path: "Actions",
        content: (corporateContact) => (
          <div className="collaboration-actions">
            <i
              className="fa fa-pencil fa-xl orange option"
              onClick={() =>
                history.push(
                  `${routeList.corporateContact.path}/${corporateContact.id}/edit`
                )
              }
            />
            <CorporateContactModal
              onExit={onExit}
              corporateContact={corporateContact}
              history={history}
              handleDelete={handleDelete}
            />
          </div>
        ),
        label: "Actions"
      }
    ];
    return columns;
  };
  render() {
    const { corporateContact } = this.props;
    return (
      <Table columns={this.COLUMNS(corporateContact)} data={corporateContact} />
    );
  }
}

export default CorporateContactTable;
