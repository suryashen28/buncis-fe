import React, { Component } from "react";
import { Field, reduxForm, formValueSelector, Form } from "redux-form";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import {
  required,
  number,
  email,
  maxLength,
  minLength
} from "@utils/form-validation";
import Input from "@components/common/input/Input";
import Select from "@components/common/select/Select";
import { getAllCorporates } from "@redux/action/corporate-actions";
import {
  resetCorporateContactForm,
  createCorporateContact,
  updateCorporateContact,
  getCorporateContactByID
} from "@redux/action/corporate-contact-actions";
import routeList from "@src/route";

class CorporateContactForm extends Component {
  componentDidMount() {
    const { getAllCorporates, getCorporateContactByID, resetForm } = this.props;
    resetForm("CorporateContactForm");
    getAllCorporates();
    !!this.getCorporateContactID() &&
      getCorporateContactByID(this.getCorporateContactID());
  }

  handleSubmit = () => {
    const {
      updateCorporateContact,
      createCorporateContact,
      resetForm,
      updatedValues,
      history
    } = this.props;
    const { corporateContact } = routeList;
    !!this.getCorporateContactID()
      ? updateCorporateContact(updatedValues, this.getCorporateContactID())
      : createCorporateContact(updatedValues);

    resetForm("CorporateContactForm");
    history.push(corporateContact.path);
  };

  getCorporateContactID = () => this.props.match.params.id;

  getFormTitle = () => (!!this.getCorporateContactID() ? "Edit" : "Add");

  render() {
    const { valid, history, corporateOptions, initialValues } = this.props;
    const renderButton = () => {
      const { corporateContact } = routeList;

      return (
        <div className="action-button">
          <button
            className="btn btn-danger btn-sm m-2 button-header"
            onClick={() => history.push(corporateContact.path)}
          >
            Cancel
          </button>
          <button
            className="btn btn-success btn-sm m-2 button-header"
            onClick={() => this.handleSubmit()}
            type="submit"
            disabled={!valid}
          >
            Submit
          </button>
        </div>
      );
    };
    const FIELDS = [
      {
        classInput: "form-control-text",
        name: "name",
        label: "Name",
        component: Input,
        type: "text",
        placeholder: "Name",
        id: "input-name",
        validate: [required]
      },
      {
        classInput: "form-control-text",
        name: "email",
        component: Input,
        type: "email",
        placeholder: "Email",
        label: "Email",
        id: "input-email",
        validate: [required, email]
      },
      {
        classInput: "form-control-text",
        name: "phoneNumber",
        component: Input,
        type: "text",
        placeholder: "Phone Number",
        label: "Phone Number",
        id: "input-phone-number",
        validate: [required, number, maxLength(12), minLength(10)]
      },
      {
        classInput: "form-control-text",
        name: "corporateId",
        type: "select",
        component: Select,
        label: "Corporate",
        title: "name",
        options: corporateOptions,
        placeholder: "Select Corporate",
        initialValues: initialValues.corporateId,
        validate: [required]
      },
      {
        classInput: "form-control-text",
        name: "position",
        component: Input,
        type: "text",
        placeholder: "Position",
        label: "Position",
        id: "input-position",
        validate: [required]
      }
    ];
    const renderFormFields = () =>
      FIELDS.map((field) => <Field key={field.name} {...field} />);

    const renderForm = () => {
      return (
        <div className="form-container">
          <Form onSubmit={this.handleSubmit}>{renderFormFields()}</Form>
        </div>
      );
    };

    return (
      <div>
        <div className="collaboration-form">
          <div className="collaboration-form-container">
            <h1 className="content-title">
              {this.getFormTitle()} Corporate Contact
            </h1>
          </div>
        </div>
        {renderForm()}
        <div>{renderButton()}</div>
      </div>
    );
  }
}

const selector = formValueSelector("CorporateContactForm");

const mapStateToProps = (state) => {
  const {
    corporate: { corporateOptions },
    loading,
    error
  } = state;
  return {
    corporateOptions,
    loading,
    error,
    initialValues: state.corporateContact.form,
    updatedValues: selector(
      state,
      "name",
      "phoneNumber",
      "corporateId",
      "position",
      "email"
    )
  };
};

const mapDispatchToProps = (dispatch) => ({
  getAllCorporates: () => dispatch(getAllCorporates()),
  createCorporateContact: (data) => dispatch(createCorporateContact(data)),
  updateCorporateContact: (data, id) =>
    dispatch(updateCorporateContact(data, id)),
  getCorporateContactByID: (id) => dispatch(getCorporateContactByID(id)),
  resetForm: () => dispatch(resetCorporateContactForm())
});

CorporateContactForm = reduxForm({
  form: "CorporateContactForm",
  multipartForm: true,
  enableReinitialize: true
})(withRouter(CorporateContactForm));

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CorporateContactForm);
