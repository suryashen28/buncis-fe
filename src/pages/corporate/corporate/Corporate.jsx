import React, { Component, lazy, Suspense } from "react";
import queryString from "query-string";
import { connect } from "react-redux";

import {
  getCorporates,
  deleteCorporate
} from "@redux/action/corporate-actions";
import CardLoading from "@components/loading/CardLoading";
import routeList from "@src/route";
import Search from "@components/common/search/Search";
import { getListOfPassedYears } from "@utils/date";
import SelectForm from "@components/common/select/SelectForm";
import PaginationLoading from "@components/loading/PaginationLoading";
import CardPageLoading from "@components/loading/CardPageLoading";

const CardCorporate = lazy(() =>
  import("@components/pages/corporate/CardCorporate")
);
const Pagination = lazy(() =>
  import("@components/common/pagination/Pagination")
);

class Corporate extends Component {
  getParams = () => {
    const { history } = this.props;
    return queryString.parse(history.location.search);
  };

  componentDidMount() {
    const { history, page } = this.props;
    const urlParam = this.getParams();
    !!urlParam.page && history.push({ search: `page=1` });
    this.getCorporates(page.page);
  }

  getCorporates = (page) => {
    const { getCorporates } = this.props;
    getCorporates({ page: page });
  };

  renderCardCorporates = () => {
    const { corporate, deleteCorporate, history, isLoading } = this.props;
    return isLoading ? (
      <CardPageLoading />
    ) : (
      corporate.map((card) => (
        <Suspense key={card.id} fallback={<CardLoading />}>
          <CardCorporate
            key={card.id}
            card={card}
            onDeleteCorporate={deleteCorporate}
            history={history}
          />
        </Suspense>
      ))
    );
  };

  isEditModeActive = () => {
    const { location, match } = this.props;
    return location.pathname !== match.path;
  };

  onFilterSubmit = (result) => {
    const { history } = this.props;
    this.setState({ ...result });
    const params = {
      page: 1,
      ...result
    };
    const urlParams = queryString.stringify(params);
    history.push({ search: urlParams });
    this.props.getCorporates(params);
  };

  render() {
    const { history, page, isLoading } = this.props;
    const { corporateForm } = routeList;
    const yearOptions = getListOfPassedYears(10);
    return (
      <div className="corporate-content">
        <div className="layout-style">
          <h2>Corporate</h2>
          <div className="position-header-content">
            <SelectForm
              options={yearOptions}
              placeholder="Select Year"
              label="Year"
              title="id"
              name="year"
              onChange={this.onFilterSubmit}
            />
            <button
              className="btn btn-reset"
              onClick={() => this.onFilterSubmit()}
            >
              Reset Filter
            </button>
            <button
              onClick={() => history.push(corporateForm.path.add)}
              className="btn btn-add"
            >
              Add New Corporate
            </button>
            <Search onSearchSubmit={this.onFilterSubmit} />
          </div>
        </div>
        <div className="card-deck">
          <div className="corporate-container">
            {this.renderCardCorporates()}
          </div>
        </div>
        <div className="card-pagination">
          <Suspense fallback={<PaginationLoading />}>
            <Pagination
              numberOfPages={page.totalPage}
              currentPage={page.page}
              history={history}
              isLoading={isLoading}
              onChange={this.getCorporates}
              name="-card"
            />
          </Suspense>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({
  corporate: { corporate, isLoading, error, page }
}) => ({
  corporate,
  isLoading,
  error,
  page
});

const mapDispatchToProps = (dispatch) => ({
  getCorporates: (params) => dispatch(getCorporates(params)),
  deleteCorporate: (id) => dispatch(deleteCorporate(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Corporate);
