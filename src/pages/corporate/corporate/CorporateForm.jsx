import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Field, reduxForm, formValueSelector } from "redux-form";

import {
  createCorporate,
  getCorporateByID,
  updateCorporate,
  resetCorporateForm
} from "@redux/action/corporate-actions";
import { required } from "@utils/form-validation";
import UploadImage from "@components/common/upload-image/UploadImage";
import Input from "@components/common/input/Input";
import TextArea from "@components/common/textarea/TextArea";
import routeList from "@src/route";

const FIELDS = [
  {
    classInput: "form-control-text",
    name: "name",
    label: "Name",
    component: Input,
    type: "text",
    placeholder: "Enter Name",
    id: "input-title",
    validate: [required]
  },
  {
    classInput: "form-control",
    name: "address",
    label: "Address",
    component: TextArea,
    type: "area",
    placeholder: "Enter Address",
    id: "input-address",
    validate: [required]
  }
];

class CorporateForm extends Component {
  componentDidMount() {
    const { getCorporateByID, resetForm } = this.props;
    resetForm("CorporateForm");
    !!this.getCorporateID() && getCorporateByID(this.getCorporateID());
  }

  handleSubmit = () => {
    const {
      updatedValues,
      createCorporate,
      updateCorporate,
      resetForm,
      history
    } = this.props;
    const { corporate } = routeList;
    const data = !!updatedValues.image
      ? { ...updatedValues, image: updatedValues.image.file }
      : updatedValues;
    const formData = new FormData();
    Object.keys(data).map((key) => formData.append(key, data[key]));
    !!this.getCorporateID()
      ? updateCorporate(formData, this.getCorporateID())
      : createCorporate(formData);

    resetForm("CorporateForm");
    history.push(corporate.path);
  };

  getCorporateID = () => this.props.match.params.id;
  getFormTitle = () => (!!this.getCorporateID() ? "Edit" : "Add");

  renderFormFields = () => FIELDS.map((f) => <Field key={f.name} {...f} />);
  renderFormButton = () => {
    const { history, valid } = this.props;
    const { corporate } = routeList;
    return (
      <div className="action-button">
        <button
          className="btn btn-danger btn-sm m-2 button-header"
          onClick={() => history.push(corporate.path)}
        >
          Cancel
        </button>
        <button
          className="btn btn-success btn-sm m-2 button-header"
          onClick={this.handleSubmit}
          disabled={!valid}
          type="submit"
        >
          Save
        </button>
      </div>
    );
  };

  renderForm = () => {
    const { initialValues } = this.props;
    const { image } = initialValues;
    return (
      <div>
        <div className="layout-style">
          <h1 className="content-title">{this.getFormTitle()} Corporate</h1>
        </div>
        <div className="form-container">
          <div className="corporate">
            <div className="upload-image">
              <Field
                classInput="form-control"
                name="image"
                component={UploadImage}
                type="file"
                initialValues={image}
              />
            </div>
            <div className="form-corporate">{this.renderFormFields()}</div>
          </div>
        </div>
        {this.renderFormButton()}
      </div>
    );
  };

  render() {
    return <div>{this.renderForm()}</div>;
  }
}

const selector = formValueSelector("CorporateForm");

const mapStateToProps = (state) => {
  return {
    initialValues: state.corporate.form,
    updatedValues: selector(state, "name", "address", "image")
  };
};

const mapDispatchToProps = (dispatch) => ({
  createCorporate: (data) => dispatch(createCorporate(data)),
  updateCorporate: (data, id) => dispatch(updateCorporate(data, id)),
  getCorporateByID: (id) => dispatch(getCorporateByID(id)),
  resetForm: () => dispatch(resetCorporateForm())
});

CorporateForm = reduxForm({
  form: "CorporateForm",
  multipartForm: true,
  enableReinitialize: true
})(withRouter(CorporateForm));

export default connect(mapStateToProps, mapDispatchToProps)(CorporateForm);
