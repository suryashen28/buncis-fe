import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Field, reduxForm, formValueSelector, reset, Form } from "redux-form";

import { required, email } from "@utils/form-validation.js";
import { login as authenticate } from "@utils/auth";
import Input from "@components/common/input/Input";
import { login } from "@redux/action/auth-actions";
import { getUser } from "@utils/auth";

import "react-toastify/dist/ReactToastify.css";

const FIELDS = [
  {
    classInput: "login-form",
    name: "email",
    component: Input,
    type: "email",
    placeholder: "subdivision@bncc.net",
    id: "input-email",
    validate: [required, email]
  },
  {
    classInput: "login-form",
    name: "password",
    component: Input,
    type: "password",
    placeholder: "Enter Password",
    id: "input-password",
    validate: [required]
  }
];

const LoginForm = (props) => {
  const { login, updatedValues, handleSubmit, resetForm } = props;

  const authenticateUser = (response) => {
    authenticate(response);
    const { subdivision } = getUser();
    props.history.push(`/${subdivision.toLowerCase()}/dashboard`);
  };

  const submitForm = async () => {
    const response = await login(updatedValues);
    !!response && authenticateUser(response);
    resetForm();
  };

  const renderFormFields = () =>
    FIELDS.map((f) => <Field key={f.name} {...f} />);

  return (
    <Form onSubmit={handleSubmit(submitForm)}>
      {renderFormFields()}
      <div className="checkbox">
        <input type="checkbox" />
        Remember Me
      </div>
      <button className="btn btn-login" type="submit">
        Login
      </button>
    </Form>
  );
};

const selector = formValueSelector("LoginForm");

const mapDispatchToProps = (dispatch) => ({
  login: (data) => dispatch(login(data)),
  resetForm: () => dispatch(reset("LoginForm"))
});

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    initialValues: state.auth.form,
    updatedValues: selector(state, "email", "password", "rememberMe")
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: "LoginForm",
    multipartForm: true,
    enableReinitialize: true
  })(withRouter(LoginForm))
);
