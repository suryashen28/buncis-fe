import React, { Component, Suspense } from "react";

import LoginForm from "@pages/login-page/LoginForm";

import { getThisYear } from "@utils/date";
import LoginImage from "@assets/images/login.svg";
import ImageLoading from "@components/loading/ImageLoading";

class LoginPage extends Component {
  render() {
    const { history } = this.props;
    return (
      <div className="login middle">
        <div className="login-image">
          <div className="login-buncis-text">
            Bunc<span>is</span>
          </div>
          <Suspense fallback={<ImageLoading />}>
            <img src={LoginImage} alt="login" />
          </Suspense>
        </div>
        <div className="login-forms">
          <div className="login-text">Log In</div>
          <LoginForm history={history} onSubmit={this.authenticateUser} />
          <div className="copyright">
            @ {getThisYear()} Bina Nusantara Computer Club
          </div>
        </div>
      </div>
    );
  }
}

export default LoginPage;
