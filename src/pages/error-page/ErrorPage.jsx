import React, { Component } from "react";

import config from "@config/index";
import httpStatusList from "@utils/http-status";
import errorDisplayImage from "@assets/images/ErrorDisplay.svg";

const host = config.app.currentHost;

class ErrorPage extends Component {
  render() {
    const { history, status, redirectTo } = this.props;
    const errorStatus = httpStatusList[status];
    return (
      <div className="error-page-container">
        <h1 className="error-title center">
          Error {errorStatus.status}-{errorStatus.message}
        </h1>
        <h3 className="error-subtitle center">{errorStatus.response}</h3>
        <img className="center" src={errorDisplayImage} alt="error" />
        <button className="btn center" onClick={() => history.push(redirectTo)}>
          Go Back
        </button>
      </div>
    );
  }
}

export default ErrorPage;
