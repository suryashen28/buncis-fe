import React, { Component } from "react";
import { Switch } from "react-router-dom";
import { connect } from "react-redux";

import Navbar from "@components/common/navbar/Navbar";
import Sidebar from "@components/common/sidebar/Sidebar";
import RoutePage from "@pages/RoutePage";

class MainPage extends Component {
  state = {
    status: {
      sidebar: "sidebar-open",
      content: "col-10"
    }
  };

  toggleSidebar = () => {
    const status = {
      sidebar:
        this.state.status.sidebar === "sidebar-open"
          ? "sidebar-close"
          : "sidebar-open",
      content: this.state.status.content === "col-10" ? "col-12" : "col-10"
    };
    this.setState({ status });
  };

  render() {
    const { state, toggleSidebar } = this;
    const { status } = state;
    const { history } = this.props;
    return (
      <div className="page-container">
        <Sidebar status={status.sidebar} history={history} />
        <div className={`${status.content} color-page-content`}>
          <Navbar onToggle={toggleSidebar} />
          <div className="container-fluid">
            <div className="mt-4">
              <Switch>
                <RoutePage />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    auth: state.auth.auth
  };
};

export default connect(mapStateToProps)(MainPage);
