import React, { lazy, Suspense } from "react";
import { Switch, Route } from "react-router-dom";

import routeList from "@src/route";
import PageLoading from "@components/loading/PageLoading";

const Dashboard = lazy(() => import("@pages/dashboard/Dashboard"));
const Corporate = lazy(() => import("@pages/corporate/corporate/Corporate"));
const CorporateForm = lazy(() =>
  import("@pages/corporate/corporate/CorporateForm")
);
const CorporateContact = lazy(() =>
  import("@pages/corporate/corporate-contact/CorporateContact")
);
const CorporateContactForm = lazy(() =>
  import("@pages/corporate/corporate-contact/CorporateContactForm")
);
const Collaboration = lazy(() =>
  import("@pages/collaboration/collaboration/Collaboration")
);
const CollaborationForm = lazy(() =>
  import("@pages/collaboration/collaboration/CollaborationForm")
);
const CollaborationType = lazy(() =>
  import("@pages/collaboration/collaboration-type/CollaborationType")
);
const CollaborationTypeForm = lazy(() =>
  import("@pages/collaboration/collaboration-type/CollaborationTypeForm")
);

const RoutePage = () => {
  return (
    <Suspense fallback={<PageLoading />}>
      <Switch>
        <Route exact path={routeList.dashboard.path} component={Dashboard} />
        <Route exact path={routeList.corporate.path} component={Corporate} />
        <Route
          path={routeList.corporateForm.path.add}
          component={CorporateForm}
        />
        <Route
          path={routeList.corporateForm.path.edit}
          component={CorporateForm}
        />
        <Route
          path={routeList.corporateContactForm.path.add}
          component={CorporateContactForm}
        />
        <Route
          path={routeList.corporateContactForm.path.edit}
          component={CorporateContactForm}
        />
        <Route
          path={routeList.corporateContact.path}
          component={CorporateContact}
        />
        <Route
          path={routeList.collaborationForm.path.edit}
          component={CollaborationForm}
        />
        <Route
          path={routeList.collaborationForm.path.add}
          component={CollaborationForm}
        />
        <Route path={routeList.collaboration.path} component={Collaboration} />
        <Route
          path={routeList.collaborationTypeForm.path.add}
          component={CollaborationTypeForm}
        />
        <Route
          path={routeList.collaborationTypeForm.path.edit}
          component={CollaborationTypeForm}
        />
        <Route
          path={routeList.collaborationType.path}
          component={CollaborationType}
        />
      </Switch>
    </Suspense>
  );
};

export default RoutePage;
