import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import MainPage from "@pages/MainPage";
import LoginPage from "@pages/login-page/LoginPage";
import ProtectedRoute from "@components/common/protected-route/ProtectedRoute";
import ErrorPage from "@pages/error-page/ErrorPage";
import routeList from "@src/route";

import "@styles/main.scss";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";
import "react-toastify/dist/ReactToastify.css";

class App extends Component {
  getRouteList = () => {
    return Object.keys(routeList).flatMap((route) =>
      !!routeList[route].path.add
        ? Object.keys(routeList[route].path).map(
            (key) => routeList[route].path[key]
          )
        : routeList[route].path
    );
  };

  render() {
    const routes = this.getRouteList();
    return (
      <React.Fragment>
        <ToastContainer autoClose={3000} />
        <Switch>
          <Redirect exact from="/" to="/login" />
          <Route path="/login" component={LoginPage} />
          <ProtectedRoute path={routes} render={() => <MainPage />} />
          <Route
            path="*"
            render={(props) => (
              <ErrorPage {...props} status={404} redirectTo={"/login"} />
            )}
          />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
